# -*- coding: utf-8 -*-
"""



    a Mobile interface definition

        adapted from robotframework-uiautomatorlibrary



"""





class IMobile():
    """

        represents an android device

    """

    def __init__(self):
        self.set_serial(None)

    def set_serial(self, android_serial):
        """
        Specify given *android_serial* device to perform test.

        You do not have to specify the device when there is only one device connects to the computer.

        When you need to use multiple devices, do not use this keyword to switch between devices in test execution.

        Using different library name when importing this library according to http://robotframework.googlecode.com/hg/doc/userguide/RobotFrameworkUserGuide.html?r=2.8.5.

        | Setting | Value |  Value |  Value |
        | Library | Mobile | WITH NAME | Mobile1 |
        | Library | Mobile | WITH NAME | Mobile2 |

        And set the serial to each library.

        | Test Case        | Action             | Argument           |
        | Multiple Devices | Mobile1.Set Serial | device_1's serial  |
        |                  | Mobile2.Set Serial | device_2's serial  |

        """


    def get_device_info(self):
        """
        Retrieve the device info.

        The keyword will return a dictionary.

        You can log the information by using the log dictionary keyword in build in Collections library(http://robotframework.googlecode.com/hg/doc/libraries/Collections.html?r=2.8.4).

        Example:
        | ${device_info} | Get Device Info |
        | Log Dictionary | ${device_info}  |

        =>

        Dictionary size is 9 and it contains following items:\n
        currentPackageName: com.android.keyguard\n
        displayHeight: 1776\n
        displayRotation: 0\n
        displaySizeDpX: 360\n
        displaySizeDpY: 640\n
        displayWidth: 1080\n
        naturalOrientation: True\n
        productName: hammerhead\n
        sdkInt: 19\n

        Or get specific information of the device by giving the key.

        | ${device_info}  | Get Device Info | | |
        | ${product_name} | Get From Dictionary | ${device_info} | productName |

        =>

        ${product_name} = hammerhead

        """


#Key Event Actions of the device
    """
    Turn on/off screen
    """
    def turn_on_screen(self):
        """
        Turn on the screen.
        """


    def turn_off_screen(self):
        """
        Turn off the screen.
        """


    """
    Press hard/soft key
    """

    def press_key(self, *keys):
        """
        Press *key* keycode.

        You can find all keycode in http://developer.android.com/reference/android/view/KeyEvent.html

        """
        #not tested


    def press_home(self):
        """
        Press home key.
        """


    def press_back(self):
        """
        Press back key.
        """

    def press_left(self):
        """
        Press left key.
        """

    def press_right(self):
        """
        Press right key.
        """

    def press_up(self):
        """
        Press up key.
        """

    def press_down(self):
        """
        Press down key.
        """

    def press_center(self):
        """
        Press center key.
        """


    def press_menu(self):
        """
        Press menu key.
        """


    def press_search(self):
        """
        Press search key.
        """


    def press_enter(self):
        """
        Press enter key.
        """


    def press_delete(self):
        """
        Press delete key.
        """

    def press_recent(self):
        """
        Press recent key.
        """

    def press_volume_up(self):
        """
        Press volume up key.
        """

    def press_volume_down(self):
        """
        Press volume down key.
        """


    def press_camera(self):
        """
        Press camera key.
        """

    def press_power(self):
        """
        Press power key.
        """

#Gesture interaction of the device

    def click_at_coordinates(self, x, y):
        """
        Click at (x,y) coordinates.
        """

    def swipe_by_coordinates(self, sx, sy, ex, ey, steps=10):
        """
        Swipe from (sx, sy) to (ex, ey) with *steps* .

        Example:
        | Swipe By Coordinates | 540 | 1340 | 940 | 1340 | | # Swipe from (540, 1340) to (940, 100) with default steps 10 |
        | Swipe By Coordinates | 540 | 1340 | 940 | 1340 | 100 | # Swipe from (540, 1340) to (940, 100) with steps 100 |
        """


# Swipe from the center of the ui object to its edge

    def swipe_left(self, steps=10, *args, **selectors):
        """
        Swipe the UI object with *selectors* from center to left.

        Example:

        | Swipe Left | description=Home screen 3 | | # swipe the UI object left |
        | Swipe Left | 5 | description=Home screen 3 | # swipe the UI object left with steps=5 |

        See `introduction` for details about identified UI object.
        """

    def swipe_right(self, steps=10, *args, **selectors):
        """
        Swipe the UI object with *selectors* from center to right

        See `Swipe Left` for more details.
        """


    def swipe_top(self, steps=10, *args, **selectors):
        """
        Swipe the UI object with *selectors* from center to top

        See `Swipe Left` for more details.
        """

    def swipe_bottom(self, steps=10, *args, **selectors):
        """
        Swipe the UI object with *selectors* from center to bottom

        See `Swipe Left` for more details.
        """

    def object_swipe_left(self, obj, steps=10):
        """
        Swipe the *obj* from center to left

        Example:

        | ${object} | Get Object | description=Home screen 3 | # Get the UI object |
        | Object Swipe Left | ${object} | | # Swipe the UI object left |
        | Object Swipe Left | ${object} | 5 | # Swipe the UI object left with steps=5 |
        | Object Swipe Left | ${object} | steps=5 | # Swipe the UI object left with steps=5 |

        See `introduction` for details about identified UI object.
        """

    def object_swipe_right(self, obj, steps=10):
        """
        Swipe the *obj* from center to right

        See `Object Swipe Left` for more details.
        """


    def object_swipe_top(self, obj, steps=10):
        """
        Swipe the *obj* from center to top

        See `Object Swipe Left` for more details.
        """

    def object_swipe_bottom(self, obj, steps=10):
        """
        Swipe the *obj* from center to bottom

        See `Object Swipe Left` for more details.
        """

    def drag_by_coordinates(self,sx, sy, ex, ey, steps=10):
        """
        Drag from (sx, sy) to (ex, ey) with steps

        See `Swipe By Coordinates` also.
        """


    #Wait until the specific ui object appears or gone

    # wait until the ui object appears
    def wait_for_exists(self, timeout=0, *args, **selectors):
        """
        Wait for the object which has *selectors* within the given timeout.

        Return true if the object *appear* in the given timeout. Else return false.
        """

    # wait until the ui object gone
    def wait_until_gone(self, timeout=0, *args, **selectors):
        """
        Wait for the object which has *selectors* within the given timeout.

        Return true if the object *disappear* in the given timeout. Else return false.
        """


    def wait_for_object_exists(self, obj, timeout=0):
        """
        Wait for the object: obj within the given timeout.

        Return true if the object *appear* in the given timeout. Else return false.
        """


    # wait until the ui object gone
    def wait_until_object_gone(self, obj, timeout=0):
        """
        Wait for the object: obj within the given timeout.

        Return true if the object *disappear* in the given timeout. Else return false.
        """


    # Perform fling on the specific ui object(scrollable)
    def fling_forward_horizontally(self, *args, **selectors):
        """
        Perform fling forward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """

    def fling_backward_horizontally(self, *args, **selectors):
        """
        Perform fling backward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """


    def fling_forward_vertically(self, *args, **selectors):
        """
        Perform fling forward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """


    def fling_backward_vertically(self, *args, **selectors):
        """
        Perform fling backward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be fling or not.
        """


    # Perform scroll on the specific ui object(scrollable)

    # horizontal
    def scroll_to_beginning_horizontally(self, steps=10, *args,**selectors):
        """
        Scroll the object which has *selectors* attributes to *beginning* horizontally.

        See `Scroll Forward Vertically` for more details.
        """


    def scroll_to_end_horizontally(self, steps=10, *args, **selectors):
        """
        Scroll the object which has *selectors* attributes to *end* horizontally.

        See `Scroll Forward Vertically` for more details.
        """


    def scroll_forward_horizontally(self, steps=10, *args, **selectors):
        """
        Perform scroll forward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """


    def scroll_backward_horizontally(self, steps=10, *args, **selectors):
        """
        Perform scroll backward (horizontally)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """


    def scroll_to_horizontally(self, obj, *args,**selectors):
        """
        Scroll(horizontally) on the object: obj to specific UI object which has *selectors* attributes appears.

        Return true if the UI object, else return false.

        See `Scroll To Vertically` for more details.
        """


    # vertical
    def scroll_to_beginning_vertically(self, steps=10, *args,**selectors):
        """
        Scroll the object which has *selectors* attributes to *beginning* vertically.

        See `Scroll Forward Vertically` for more details.
        """


    def scroll_to_end_vertically(self, steps=10, *args, **selectors):
        """
        Scroll the object which has *selectors* attributes to *end* vertically.

        See `Scroll Forward Vertically` for more details.
        """


    def scroll_forward_vertically(self, steps=10, *args, **selectors):
        """
        Perform scroll forward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        Example:
        | ${can_be_scroll} | Scroll Forward Vertically | className=android.widget.ListView |  | # Scroll forward the UI object with class name |
        | ${can_be_scroll} | Scroll Forward Vertically | 100 | className=android.widget.ListView | # Scroll with steps |
        """


    def scroll_backward_vertically(self, steps=10, *args, **selectors):
        """
        Perform scroll backward (vertically)action on the object which has *selectors* attributes.

        Return whether the object can be Scroll or not.

        See `Scroll Forward Vertically` for more details.
        """


    def scroll_to_vertically(self, obj, *args,**selectors):
        """
        Scroll(vertically) on the object: obj to specific UI object which has *selectors* attributes appears.

        Return true if the UI object, else return false.

        Example:

        | ${list} | Get Object | className=android.widget.ListView |  | # Get the list object |
        | ${is_web_view} | Scroll To Vertically | ${list} | text=WebView | # Scroll to text:WebView. |
        """


#Screen Actions of the device

    def get_screen_orientation(self):
        """
        Get the screen orientation.

        Possible result: natural, left, right, upsidedown

        See for more details: https://github.com/xiaocong/uiautomator#screen-actions-of-the-device
        """


    def set_screen_orientation(self, orientation):
        """
        Set the screen orientation.

        Input *orientation* : natural or n, left or l, right or r, upsidedown (support android version above 4.3)

        The keyword will unfreeze the screen rotation first.

        See for more details: https://github.com/xiaocong/uiautomator#screen-actions-of-the-device

        Example:

        | Set Screen Orientation | n | # Set orientation to natural |
        | Set Screen Orientation | natural | # Do the same thing  |
        """


    def freeze_screen_rotation(self):
        """
        Freeze the screen auto rotation
        """


    def unfreeze_screen_rotation(self):
        """
        Un-Freeze the screen auto rotation
        """


    def screenshot(self, scale=None, quality=None):
        """
        Take a screenshot of device and log in the report with timestamp, scale for screenshot size and quality for screenshot quality
        default scale=1.0 quality=100
        """


#Watcher
    def __unicode_to_dict(self, a_unicode):
        a_dict = dict()
        dict_item_count = a_unicode.count('=')
        for count in range(dict_item_count):
            equal_sign_position = a_unicode.find('=')
            comma_position = a_unicode.find(',')
            a_key = a_unicode[0:equal_sign_position]
            if comma_position == -1:
                a_value = a_unicode[equal_sign_position + 1:]
            else:
                a_value = a_unicode[equal_sign_position + 1:comma_position]
                a_unicode = a_unicode[comma_position + 1:]
            a_dict[a_key] = a_value
        return a_dict

    def register_click_watcher(self, watcher_name, selectors, *condition_list):
        """
        The watcher click on the object which has the *selectors* when conditions match.
        """

    def register_press_watcher(self, watcher_name, press_keys, *condition_list):
        """
        The watcher perform *press_keys* action sequentially when conditions match.
        """

    def remove_watchers(self, watcher_name = None):
        """
        Remove watcher with *watcher_name* or remove all watchers.
        """

    def list_all_watchers(self):
        """
        Return the watcher list.
        """


#Selector

    def get_object(self, *args, **selectors):
        """
        Get the UI object with selectors *selectors*

        See `introduction` for details about identified UI object.
        """


    def get_count(self, *args, **selectors):
        """
        Return the count of UI object with *selectors*

        Example:

        | ${count} | Get Count | text=Accessibility | # Get the count of UI object text=Accessibility |
        | ${accessibility_text} | Get Object | text=Accessibility | # These two keywords combination |
        | ${count} | Get Count Of Object | ${accessibility_text} | # do the same thing. |

        """


#     def get_count_of_object(self, obj):
#         """
#         Return the count of given UI object
#
#         See `Get Count` for more details.
#         """
#         return len(obj)

    def get_info_of_object(self, obj, selector=None):
        """
        return info dictionary of the *obj*
        The info example:
        {
         u'contentDescription': u'',
         u'checked': False,
         u'scrollable': True,
         u'text': u'',
         u'packageName': u'com.android.launcher',
         u'selected': False,
         u'enabled': True,
         u'bounds':
                   {
                    u'top': 231,
                    u'left': 0,
                    u'right': 1080,
                    u'bottom': 1776
                   },
         u'className': u'android.view.View',
         u'focusable': False,
         u'focused': False,
         u'clickable': False,
         u'checkable': False,
         u'chileCount': 1,
         u'longClickable': False,
         u'visibleBounds':
                          {
                           u'top': 231,
                           u'left': 0,
                           u'right': 1080,
                           u'bottom': 1776
                          }
        }
        """


    def click(self, *args, **selectors):
        """
        Click on the UI object with *selectors*

        | Click | text=Accessibility | className=android.widget.Button | # Click the object with class name and text |
        """


    def long_click(self, *args, **selectors):
        """
        Long click on the UI object with *selectors*

        See `Click` for more details.
        """


    def call(self, obj, method, *args, **selectors):
        """
        This keyword can use object method from original python uiautomator

        See more details from https://github.com/xiaocong/uiautomator

        Example:

        | ${accessibility_text} | Get Object | text=Accessibility | # Get the UI object |
        | Call | ${accessibility_text} | click | # Call the method of the UI object 'click' |
        """


    def set_text(self, input_text, *args, **selectors):
        """
        Set *input_text* to the UI object with *selectors*
        """

# Other feature

    def clear_text(self, *args, **selectors):
        """
        Clear text of the UI object  with *selectors*
        """


    def open_notification(self):
        """
        Open notification

        Built in support for Android 4.3 (API level 18)

        Using swipe action as a workaround for API level lower than 18

        """


    def open_quick_settings(self):
        """
        Open quick settings

        Work for Android 4.3 above (API level 18)

        """


    def sleep(self, time):
        """
        Sleep(no action) for *time* (in millisecond)
        """

    def install(self, apk_path):
        """
        Install apk to the device.

        Example:

        | Install | ${CURDIR}${/}com.hmh.api_4.0.apk | # Given the absolute path to the apk file |
        """


    def uninstall(self, package_name):
        """
        Uninstall the APP with *package_name*
        """


    def execute_adb_command(self, cmd):
        """
        Execute adb *cmd*
        """


    def execute_adb_shell_command(self,cmd):
        """
        Execute adb shell *cmd*
        """


    def type(self, input_text):
        """
        [IME]

        Type *text* at current focused UI object
        """


    def start_test_agent(self):
        """
        [Test Agent]

        Start Test Agent Service
        """


    def stop_test_agent(self):
        """
        [Test Agent]

        Stop Test Agent Service
        """


    def connect_to_wifi(self, ssid, password=None):
        """
        [Test Agent]

        Connect to *ssid* with *password*
        """


    def clear_connected_wifi(self):
        """
        [Test Agent]

        Clear all existed Wi-Fi connection
        """

    # new keywords
    def wait_update(self,timeout=3000):
        """

        wait for the screen to change

        :param timeout: int , max milliseconds to wait
        :return:
        """

    def wait_idle(self,timeout=3000):
        """

        wait for the to become idle

        :param timeout:
        :return:
        """
        
    # added 2014/01/16
    def dump(self,filepath=""):
        """

        dump the xml hierarchy of the current screen

        :param filepath: str
        :return:
        """

    # DEPRECATED use get_object instead
    # def get_object_data(self, **selectors):
    #     """
    #         get properties of the ui object designated by the selectors
    #
    #     :param selectors:
    #     :return: dict of ui object properties
    #     """

    # add indexpath keywords (rado)
    # Indexpath integration
    def get_info_by_index(self, index, selector, dump_file=None):
        """
        Return the data contained by the attribute specified by 'selector'.
        """


    def click_by_index (self, index, dump_file=None, **selectors):
        """
        Makes a click on the given element specified by its index path
        """


    def long_click_by_index(self, index, dump_file=None, **selectors):
        """
        Makes a long click on the given element specified by its index path
        """