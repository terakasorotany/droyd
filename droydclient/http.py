__author__ = 'cocoon'
"""

    HTTP api to droyd hub


        open_session
        close_session

        adb_devices

        + all device operations ( click ,press_home ...)




"""

import sys
import json
import requests

from interface import IMobile as mobile_factory

# from droydrunner.utils.store import Store as EmbededStore
# from droydrunner.api.server.droyd_relay import DroydRelay
# from droydrunner.uihub import UiHub


# default log_cb
stdout_no_buf = True

def out(msg,nl=True):
    """
        send message to stdout and flush
    """
    if nl:
        # newline : print msg + nl
        print msg
    else:
        # send without new line
        print msg,

    if stdout_no_buf:
        sys.stdout.flush()



# class Store(EmbededStore):
#     """
#
#     """
#
#     def setup(self):
#         pass


class PhoneSession():
    """

    """
    def __init__(self):
        """
        """
        self._store = {
            'accounts' : {},
            'current':  {}
        }



    def add_users(self,*accounts):
        """

        """
        for k in accounts:
            self._store['accounts'][k]= {}
            #self._store.add_entity('main',k,v)


    def current_session(self,data=None):
        """
            current session info
        """
        if not data:
            # read it
            return self._store['current']
        else:
            # set data
            self._store['current'] = data

    def close(self):
        """
        """
        self._store = {
            'accounts' : {},
            'current':  {}
        }








class HttpDroydHub(object):
    """

    """

    def __init__(self,url):
        """

        :param url:
        :return:
        """
        # the base url eg localhost:5000
        self._url = url

        self._mobile_factory = mobile_factory

        # set the requests session
        self._web_session=requests.Session()
        self._web_session.headers.update({
            'content-type': 'application/json; charset=utf-8',
            'accept': 'application/json',
            'Accept-Charset': 'utf-8'
            })


        # the phone session
        self._session = None


    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """

        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        #self._store.close()
        if self._session:
            self._session.close()

    def url(self, uri = None):
        """

        :param complement:
        :return:
        """
        if not uri:
            return self._url
        else:
            #return "/".join(self._url , uri )
            return "%s%s" % (self._url,uri)



    def open_session(self,* device_ids):
        """

            open a phone session:
                store accounts
        :param accounts:
        :return:
        """
        if self._session:
            return self._session
        # save session data
        self._session = PhoneSession()
        self._session.add_users(* device_ids)

        # request to server
        data = json.dumps( list( device_ids))
        response = self._web_session.post(self.url('/sessions'),data=data)

        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        self._session.current_session(response_data)
        return


    def close_session(self):
        """
            close current session

        :param session_id:
        :return:
        """
        session_name = self._session.current_session()['session']
        uri = '/sessions/%s' % session_name
        response = self._web_session.delete(self.url(uri))

        self._session.close()
        return response


    def _handle_response(self,response):
        """
            handle the http response

        :param response: flask response object
        :return:  json_data or raise runtime error if http error encountered
        """
        # response analyse
        if response.status_code >= 200 and response.status_code < 300:
            # reponse ok
            try:
                json_data = response.json()
                result = json_data['result']
            except:
                result = None
        else:
            # other http result (404 , 500 )
            raise RuntimeError('failed: status:%s' % str(response.status_code))

        return result



    def adb_devices(self):
        """


        :return: list of android connected devices
        """
        url =  self._url + "/adb/*/devices/"
        print "POST %s" % url
        response = self._web_session.post(url)

        return self._handle_response(response)

        # # response analyse
        # if response.status_code >= 200 and response.status_code < 300:
        #     # reponse ok
        #     try:
        #         json_data = response.json()
        #         result = json_data['result']
        #     except:
        #         result = None
        # else:
        #     # other http result (404 , 500 )
        #     raise RuntimeError('failed: status:%s' % str(response.status_code))
        #
        # return result

    def scan_devices(self):
        """
         list the connected devices and return info (list)

            :return:
                - nb (int) number of connected devices
                - device_names (list)  a list of connected device names
                - device_status (dict) the status of devices
                - tags  a list of names ( userA , userB ... to set tags )

        """

        device_list= self.adb_devices()

        nb=0
        device_names= []
        device_status= []
        tags= []

        nb = len(device_list)
        if nb:
            for i in xrange(0,nb):
                device_names.append(device_list[i][0])
                device_status.append(device_list[i][1])
                tags.append('user%d' % i)
        return nb,device_names,device_status,tags




    def __getattr__(self, item):
        """

        :param item:
        :return:
        """
        # call a function
        def wrapper(device_id,*args,**kwargs):
            """

            """
            function_name = item
            #mobile = self._mobiles[device_id]
            # check if method available for this device
            #try:
            #    function = getattr(mobile,function_name)
            #except AttributeError,e:
            #    raise e

            # compose request
            return self._request(device_id,function_name,*args,**kwargs)

        return wrapper


    def _request(self,device_id,operation_name, *args,**kwargs):
        """
            launch a request

        :param device_id:
        :param args:
        :param kwargs:
        :return:
        """
        url =  self._url + "/agents/%s/%s/" % (str(self._session.current_session()[device_id]), operation_name)
        print "POST " + url + " " + str(args) + "  " + str(kwargs)
        response = self._web_session.post(url,data=json.dumps(kwargs))

        # response analyse
        if response.status_code >= 200 and response.status_code < 300:
            # reponse ok
            #response.encoding="utf-8"
            #response.encoding = 'ISO-8859-1'
            try:
                #data= response.text
                #result=json.loads(data)

                json_data = response.json()
                result = json_data['result']
            except:
                result = None
        else:
            # other http result (404 , 500 )
            json_data = response.json()
            # print json_data['message']
            # print json_data['exc_type']
            # print json_data['exc_value']

            text =[]
            text.append(json_data['message'])
            text.append(json_data['exc_type'])
            text.append(json_data['exc_value'])


            #raise RuntimeError('failed: status:%s' % str(response.status_code))
            raise RuntimeError('failed: status:%s , message: %s' % (str(response.status_code),"  ".join(text)))

        return result



HttpClient=HttpDroydHub



