#from distutils.core import setup
from setuptools import setup

setup(
    name='droydscripter',
    version='0.2.3',
    packages=[ 'droydscripter'],
    package_data = { "droydscripter": [ "*.yml" ] },
    #install_requires=['docopt', 'PyYAML'],
    entry_points={
        'console_scripts': [
            'droydscript = droydscripter.droydscript:main',
        ]
    },
    url='',
    license='',
    author='cocoon',
    author_email='tordjman.laurent@gmail.com',
    description='tools for android test automation'
)