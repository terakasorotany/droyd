__author__ = 'cocoon'


from droydrunner.uidevice import UiDevice
import time


# white gs5
id1 = '482cf4c2'


def display(msg):
    print msg.encode('utf8')


def test_ussd_125():


    # create uidevice
    d1 = UiDevice(serial=id1)

    # select phone app
    d1.quick_launch('phone')

    # call #125#
    d1.phone.call_destination("#125#")

    # get the message USSD code running
    message = d1.device(resourceId= 'android:id/message', className = 'android.widget.TextView').text
    display( message)


    # wait to see OK button
    d1.device(resourceId='android:id/button1').wait.exists(timeout=3000)
    #time.sleep(3)

    # we got the screen

    # get the message
    message = d1.device(resourceId= 'android:id/message', className = 'android.widget.TextView').text
    display( message )

    # wait to see message
    time.sleep(3)

    # press OK
    button = d1.device(resourceId='android:id/button1')
    button.click()


    return






if __name__=='__main__':


    test_ussd_125()


