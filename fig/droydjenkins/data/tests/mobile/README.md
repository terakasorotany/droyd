iva
===


tools for iva by soft

integrated test environment build upon

* docker 
* robotframework
* jenkins

maintools
--------

* droydrunner: test for mobile devices
* syprunner: tests for voip



build a test env 
----------------

clone the repository on a docker host

customize the directory name 
	* iva to OVC 

customize the /jenkins base state
* by default the robot plugin is installed


customize the /tests directory
	by default the robotdemo tests are installed with a jobs.yaml
	add your tests and jobs.yaml


customize the fig.yml
* especialy jenkins.ports  change 8087 to another port for the jenkins server


build the data image

```
fig build
```
will create a iva_data_1 docker image


use
---

start the services
------------------

```
fig up
```

will start 2 containers

iva_data_1 : for the data volumes /jenkins and /tests
iva_jenkins_1:  start a kenkins server with the data of iva_data


stop the service 
```
fig stop
```



open a console on the data :

```
sudo docker run -ti -rm --name iva_console --volumes-from iva_data_1 cocoon/python /bin/bash
```

jenkins state is under /jenkins

tests are under /tests


















