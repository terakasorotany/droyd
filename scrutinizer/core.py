# -*- coding: utf8 -*-
__author__ = 'cocoon'


from xml.etree import ElementTree as ET
import json
import hashlib
import base64



class UiNode(object):
    """
        a node of an xml dump

    """
    _converter= { 'className': 'class', 'resourceId': 'resource-id'}
    _converter_reversed= {}

    def __init__(self,element):
        """
        """
        self.element = element

    def short_id(self):
        """
            return a short name

            can be
                1) resourceId without the packageName    (com.android.incallui:id/bluetoothButton -> bluetoothButton)
        """
        short = None

        resourceId = self.element.attrib['resource-id']
        packageName = self.element.attrib['package']

        package_id = "%s:id/" % packageName

        if package_id in resourceId:
            # make a short id
            short = resourceId[len(package_id):]
        else:
            # cannot make a short id
            short = resourceId
        return short


    def class_id(self):
        """
            return a class,index identifier
        :return:
        """
        className = self.element.attrib['class']
        index = self.element.attrib['index']
        return "%s,%s" % (className,index)


    def resource_class_id(self):
        """
            return a resource_id,class,index identifier
        :return:
        """
        resourceId= self.element.attrib['resource-id']
        className = self.element.attrib['class']
        index = self.element.attrib['index']
        return "%s,%s,%s" % (resourceId,className,index)


    def hash(self):
        """
            return a hash of all attributes key:value and number of children

        """
        sig = hashlib.md5()
        # compute hash for attribs
        for k,v in self.element.attrib.iteritems():
            h = u"%s:%s;" % (k,v)
            #h = unicode.encode(h,encoding='utf-8')
            sig.update(h.encode('utf8'))
        # add hash for len
        h = str(len(self.element))
        sig.update(h)
        return sig.hexdigest()


    def hint(self):
        """
            return the first non-null content-desc or text field
        """
        for e in self.element.iter():

            if e.attrib['content-desc']:
                return e.attrib['content-desc']
            if e.attrib['text']:
                return e.attrib['text']
        return None

    def dump_attrib(self,ui_selector_name):
        """
            convert a ui attribute name into a name of attribute in hierarchy
            eg  className --> class  ,
        """
        try:
            return self._converter[ui_selector_name]
        except KeyError:
            # assume same name
            return ui_selector_name

    def ui_attrib(self,dump_selector_name):
        """
            convert a dump xml attribute name into a ui attribute name
            eg class -> className

        """
        if not self._converter_reversed:
            for k,v in self._converter.iteritems():
                self._converter_reversed[v]= k
        try:
            return self._converter_reversed[dump_selector_name]
        except KeyError:
            # assume same name
            return dump_selector_name

class UiNodes():
    """
        a set of nodes
    """

    def __init__(self,nodes):
        """
            a list of elements
        """
        self._nodes= nodes


    def uniques(self,nodes):
        """

        """
        by_resource = {}
        by_class = {}
        by_resource_class= {}

        by_text = {}
        by_content= {}

        others =[]


        for node in nodes:
            # search unique resource id
            resource_id = node.attrib['resource-id']
            if not by_resource.has_key(resource_id):
                by_resource[resource_id]= 1
            else:
                 by_resource[resource_id]+= 1

            # search unique class/index
            classId = "%s,%s" % (node.attrib['class'],node.attrib['index'])
            if not by_class.has_key(classId):
                by_class[classId] = 1
            else:
                by_class[classId] += 1


            # search unique  resource_id/class/index
            rid = "%s,%s,%s" % (node.attrib['resource-id'],node.attrib['class'],node.attrib['index'])
            if not by_resource_class.has_key(rid):
                by_resource_class[rid] = 1
            else:
                by_resource_class[rid] += 1


            # search unique text
            text = node.attrib['text']
            if not by_text.has_key(text):
                by_text[text]= 1
            else:
                 by_text[text]+= 1

            # search unique content-description
            text = node.attrib['content-desc']
            if not by_content.has_key(text):
                by_content[text]= 1
            else:
                 by_content[text]+= 1

        result = dict( by_resource=by_resource,by_class=by_class,by_resource_class=by_resource_class,  by_text=by_text,by_content=by_content)
        return result



    def identifiers(self,nodes):
        """

        """
        result= { 'uniques':[], 'others':[]}

        uniques= self.uniques(nodes)

        for e in nodes:
            node= UiNode(e)
            resource_id= e.attrib['resource-id']
            if uniques['by_resource'][resource_id] == 1:
                #resource_id is unique
                selector= {'resource-id': resource_id}
                #slug= resource_id
                id= resource_id
                slug=node.short_id()
                hint= node.hint()
                #print "slug: %s , selector: %s , hint=%s" % ( slug, selector, hint  )
                result['uniques'].append({'slug':slug,'id': id,'selector':selector,'hint':hint })
            else:
                if uniques['by_class'][node.class_id()] == 1:
                    # class_index is unique
                    selector= {'class': e.attrib['class'], 'index': e.attrib['index']}
                    #slug= node.class_id()
                    id= node.short_id()
                    slug= node.hint()
                    hint= node.hint()
                    #print "slug: %s , selector: %s , hint=%s" % ( slug, selector,hint   )
                    result['uniques'].append({'slug':slug,'id': id,'selector':selector,'hint':hint })

                else:
                    resource_class_id= node.resource_class_id()
                    if uniques['by_resource_class'][resource_class_id] == 1:
                        # resource_class_index is unique
                        selector= { 'resource-id': e.attrib['resource-id'], 'class': e.attrib['class'], 'index': e.attrib['index']}
                        #slug= node.class_id()
                        slug= resource_class_id
                        id= resource_class_id
                        slug= node.hint()
                        hint= node.hint()
                        #print "slug: %s , selector: %s , hint=%s" % ( slug, selector,hint   )
                        result['uniques'].append({'slug':slug,'id': id,'selector':selector,'hint':hint })
                    else:
                        result['others'].append(e)

                continue

        return result


    def selectors(self,nodes):
        """

        """
        identifiers=self.identifiers(nodes)

        slugs=[]
        hits= 0
        entries={}

        for e in identifiers['uniques']:
            if not e['slug'] in slugs:
                entries[e['slug']]= e
            else:
                # not unique slug
                hits+=1
                new_slug= "%s_%d" %( e['slug'] , hits)
                entries[e[new_slug]]= e

        return entries

    def selector_documentation(self,nodes):
        """

        """
        lines=[]
        lines.append(';name\n:selector')
        selectors=self.selectors(nodes)

        for name,value in selectors.iteritems():
            line= ";%s\n:%s" % (name,value['selector'])
            lines.append(line)


        return "\n".join(lines)

    def selector_documentation_table(self,nodes):
        """

        """
        lines=[]
        lines.append('|!name |!selector |!hint |')
        selectors=self.selectors(nodes)

        for name,value in selectors.iteritems():
            line= "|%s|%s|%s|" % (name,value['selector'],value['hint'])
            # transform simple quote to double quote
            line=line.replace("'",'"')
            lines.append(line)

        return "\n".join(lines)

    def selector_documentation_table_json(self,nodes):
        """

        """
        lines=[]
        lines.append('|!name |!selector |!hint |')
        selectors=self.selectors(nodes)

        for name,value in selectors.iteritems():
            line= "|%s|%s|%s|" % (name,json.dumps(value['selector']),value['hint'])
            # transform simple quote to double quote
            #line=line.replace("'",'"')
            lines.append(line)

        return "\n".join(lines)


    def make_tiddler(self,text,title="newtiddler",tags=None):
        """



        """
        tiddlers=[]
        tiddlers.append("title: %s" % title)
        tiddlers.append("tags: ")
        tiddlers.append("")
        tiddlers.append(text)
        return "\n".join(tiddlers)

    def make_image_tiddler(self,png,title=None):
        """
            title: dump_8308023266330664849.png
            type: image/png

        """
        if not title:
            title= "png"

        tiddlers=[]
        tiddlers.append("title: %s" % title)
        tiddlers.append("type: image/png")
        tiddlers.append("tags: ")
        tiddlers.append("")
        with open(png,"rb") as fh:
            data= fh.read()
            encoded= base64.b64encode(data)
            tiddlers.append(encoded)
        return "\n".join(tiddlers)


class Scrutinizer(object):
    """

        a class around an xml dump of an android screen


    """

    def __init__(self, root ):
        """

        :param element:
        :return:
        """
        self.root = root

        # compute hash table of all elements
        self._elements = self.hashes()


    @classmethod
    def from_file(cls,filename):
        """


        :param filename:
        :return:
        """
        xml = ET.parse(filename)
        root = xml.getroot()
        return cls(root)


    def hashes(self):
        """

        :return: a dict of hashes element
        """
        _elements = {}
        for element in self.root.iter():
            hash = UiNode(element).hash()
            _elements[hash] = element
        return _elements


    def iter(self,root=None, check=None,filter=None):
        """

            iter from a root element applying filter and checks


        :param root:
        :param filter_on:
        :return:
        """
        if root is None:
            root = self.root
        if check is None:
            check = []
        else:
            if isinstance(check,str):
                check = [check,]
        if filter is None:
            filter={}
        for element in root.iter():
            # checks ( clickable or long-clickables or  selected enabled , checked )
            any_check = False
            if len(check):
                # there is at least a check so verify at least one os one
                for c in check:
                    if c in element.attrib:
                        if element.attrib[c] == 'true':
                            any_check = True
                            break
            else:
                # no check : continue with it
                any_check = True
            if any_check:
                # filter it
                filter_hits = 0
                for filter_name,filter_value in filter.iteritems():
                    if filter_name in element.attrib:
                        if filter_value == element.attrib[filter_name]:
                            filter_hits += 1
                            continue
                        else:
                            break
                    else:
                        break
                if filter_hits == len(filter)  :
                     # all filters are matched
                     yield element
                continue


    def has_unique_resource_id(self,resource_id):
        """

        :param selectors:
        :return:
        """
        if not resource_id:
            return False

        r = self.root.findall(".//*[@resource-id='%s']" % resource_id)

        if len(r) == 1 :
            return True
        else:
            return False

    def has_unique_class_index(self,classname,index):
        """

        :param classname:
        :param index:
        :return:
        """
        r = self.root.findall(".//*[@class='%s']" % classname )
        count=0
        for e in r:
            if e.attrib['index'] == str(index):
                count+=1
        if count == 1:
            return True
        else:
            return False

    def has_unique_text(self,text):
        """

        :param text:
        :return:
        """
        if not text:
            return False

        count= 0
        for e in self.root.iter():
            if e.tag == 'node':
                value= e.attrib['text']
                if text == value:
                    count+=1
        if count == 1 :
            return True
        else:
            return False

    def has_unique_description(self,text):
        """

        :param text:
        :return:
        """
        if not text:
            return False

        count= 0
        for e in self.root.iter():
            if e.tag == 'node':
                value= e.attrib['content-desc']
                if text == value:
                    count+=1
        if count == 1 :
            return True
        else:
            return False



    def uniques(self,nodes):
        """

            scan nodes to find unique elements
                1) resource: unique if resource-id is unique

                2) class: unique if class an ine-dex are uniques


        :param nodes: array of etree element
        :return:
        """
        by_resource = {}
        by_class = {}
        others =[]


        for node in nodes:
            # search unique resource id
            resource_id = node.attrib['resource-id']
            if self.has_unique_resource_id(node.attrib['resource-id']):
                by_resource[resource_id] = node
            else:
                # search unique class/index
                if self.has_unique_class_index(node.attrib['class'],node.attrib['index']):
                    classId = "%s,%s" % (node.attrib['class'],node.attrib['index'])
                    by_class[classId] = node
                else:
                    others.append(node)
        result = dict( by_resource=by_resource,by_class=by_class,others=others)
        return result



class Searcher(Scrutinizer):
    """
        a high level scrutinizer
    """


    textview_class = 'android.widget.TextView'
    tab_patterns = ['android.app.ActionBar$Tab',]


    def find_tabs(self,root=None,selected=None):
        """
            iter to find tabs
                if selected is None: yield all tabs
                            is true: only selected tabs
                            is false only unselected tabs


        :return:
        """
        #tab_patterns = ['android.app.ActionBar$Tab',]

        tabs = []

        # collect all tabs
        for tab in self.tab_patterns:
            r = list(self.iter( root=root,filter = {'class':tab}))
            tabs.extend(r)
        if selected is None:
            # return all tabs
            return tabs
        else:
            # filter tabs
            if selected is True:
                sel = 'true'
            else:
                sel = 'false'
            r = [ e for e in tabs if e.attrib['selected']== sel ]
            return r


    def find_first_textview(self,element):
        """

            starting with element , find the first child with class text view

        :param element:
        :return:
        """
        try:
            child = element[0]
            if child.attrib['class'] == self.textview_class:
                return child
            else:
                # try deeper
                return self.find_first_textview(child)

        except IndexError:
            # no more children
            return None







