__author__ = 'cocoon'


import codecs
from core import Scrutinizer, Searcher, UiNode , UiNodes



def test_iter():


    s = Scrutinizer.from_file(filename)


    # get all clickables
    for clickable in s.iter(check='clickable'):
        print "%s: class=%s [%s]" %  (clickable.attrib['resource-id'] ,clickable.attrib['class'],clickable.attrib['index'])


    return


def test_unique():
    """


    :return:
    """

    #s = Scrutinizer.from_file(filename)
    s = Scrutinizer.from_file(keypad)

    # get all clickables
    clickables = list(s.iter(check=['clickable','long-clickables']))


    for e in clickables:
        unique = s.has_unique_resource_id(e.attrib['resourceId'])
        print unique


    uniques = s.uniques(clickables)

    print "unique with resource id"
    for unique in uniques['by_resource'].values():
        print UiNode(unique).short_id()
        #print UiNode(unique).hash()

    print "unique with class,index"
    for unique in uniques['by_class'].values():
        print UiNode(unique).class_id()
        #print UiNode(unique).hash()

    print "others"
    for e in uniques['others']:
        print e , e.attrib

    return


def test_selected():
    """

    :return:
    """
    #s = Scrutinizer.from_file(filename)
    s = Scrutinizer.from_file(keypad)

    # find tab selected
    selected = list(s.iter(check='selected', filter = {'class':'android.app.ActionBar$Tab'}))
    for s in selected:
        print "%s [%s]  %s" % ( s.attrib['class'], s.attrib['index'],  s.attrib['resource-id'] )

    return


def test_searcher():
    """


    :return:
    """
    s = Searcher.from_file(keypad)
    all_tabs = s.find_tabs()
    assert len(all_tabs) == 4

    selected = s.find_tabs(selected= True)
    assert len(selected) == 1

    passive = s.find_tabs(selected= False)
    assert len(passive) == 3


def test_find_first_textview():


    s = Searcher.from_file(more_menu)

    # find a list view
    filter = { 'class' : 'android.widget.ListView' , 'index' :'0' }
    start = list(s.iter(filter= filter))[0]

    t = s.find_first_textview(start)

    assert t.attrib['text'] == 'Speed dial'

    n = s.find_first_textview(t)
    assert n == None



    return


def test_dump_catalog():
    """

        scan a dump

        list all clickables

        display all uniques clickables

        for non unique  clickables:
            find if clickable + index is unique


    :return:
    """
    s = Searcher.from_file(cineday)


    # get all clickables
    clickables = list(s.iter(check=['clickable','long-clickables']))

    for e in clickables:
        unique = s.has_unique_resource_id(e.attrib['resource-id'])
        print unique,e.attrib['resource-id'], e.attrib
        continue

    print

    for e in clickables:
        unique = s.has_unique_class_index(e.attrib['class'],e.attrib['index']), e.attrib['class'],e.attrib['index'],e.attrib
        print unique

    print
    for e in clickables:
        print "text: <%s>" % e.attrib['text']
        unique = s.has_unique_text(e.attrib['text'])
        print unique,e.attrib['text'], e.attrib
        continue

    print
    for e in clickables:
        print "content-desc: <%s>" % e.attrib['content-desc']
        unique = s.has_unique_text(e.attrib['content-desc'])
        print unique,e.attrib['content-desc'], e.attrib
        continue


    # get all clickables
    uniques = s.uniques(clickables)

    print
    print "unique with resource id"
    print "-----------------------"
    for unique in uniques['by_resource'].values():
        print UiNode(unique).short_id()
        #print UiNode(unique).hash()
    print
    print "unique with class,index"
    print "-----------------------"
    for unique in uniques['by_class'].values():
        print UiNode(unique).class_id(),unique.attrib
        #print UiNode(unique).hash()

    print
    print "others"
    print "-----------------------"
    for e in uniques['others']:
        print e ,e.attrib



    for e in clickables:

        o = UiNode(e)
        print o.hint() , e.attrib
        continue



    print
    print("uniques clickables")
    print
    nodes = UiNodes(clickables)
    uniques= nodes.uniques(clickables)
    for e in clickables:
        node= UiNode(e)
        resource_id= e.attrib['resource-id']
        if uniques['by_resource'][resource_id] == 1:
            #resource_id is unique
            selector= {'resource-id': resource_id}
            #slug= resource_id
            slug=node.short_id()
            hint= node.hint()
            print "slug: %s , selector: %s , hint=%s" % ( slug, selector, hint  )
        else:
            if uniques['by_class'][node.class_id()] == 1:
                # class_index is unique
                selector= {'class': e.attrib['class'], 'index': e.attrib['index']}
                #slug= node.class_id()
                slug= node.short_id()
                slug= node.hint()
                hint= node.hint()
                print "slug: %s , selector: %s , hint=%s" % ( slug, selector,hint   )

            else:
                resource_class_id= node.resource_class_id()
                if uniques['by_resource_class'][resource_class_id] == 1:
                    # resource_class_index is unique
                    selector= { 'resource-id': e.attrib['resource-id'], 'class': e.attrib['class'], 'index': e.attrib['index']}
                    #slug= node.class_id()
                    slug= resource_class_id
                    slug= node.hint()
                    hint= node.hint()
                    print "slug: %s , selector: %s , hint=%s" % ( slug, selector,hint   )


            continue


    identifiers= nodes.identifiers(clickables)

    selectors= nodes.selectors(clickables)


    documentation= nodes.selector_documentation_table_json(clickables)


    tiddler= nodes.make_tiddler(documentation,title='clickables')

    #file = codecs.open("temp", "w", "utf-8")
    with codecs.open("produce_tiddler.tid","w",encoding="utf-8") as fh:
        fh.write(tiddler)

    print documentation

    img= nodes.make_image_tiddler(cineday_img)
    with open("produce_img_tiddler.tid","w") as fh:
        fh.write(img)


    return


def test_nodes():

    s = Searcher.from_file(cineday)


    # get all clickables
    clickables = list(s.iter(check=['clickable','long-clickables']))

    nodes = UiNodes(clickables)

    counts = nodes.uniques(clickables)


    return






if __name__=='__main__':


    filename = '../../uisnapshots/dump_incallui.uix'
    keypad = '../../uisnapshots/dump_contacts.keypad.uix '

    more_menu = "../../uisnapshots/android_4.2.4/phone/contacts.keypad_more.uix"

    cineday= "/Users/cocoon/Documents/hgclones/droyd/droydscripter/samples/cineday/dump_8308023266330664849.uix"
    cineday_img= "/Users/cocoon/Documents/hgclones/droyd/droydscripter/samples/cineday/dump_8308023266330664849.png"


    test_nodes()

    test_dump_catalog()

    # test_iter()
    # test_unique()
    # test_selected()
    # test_searcher()
    # test_find_first_textview()
