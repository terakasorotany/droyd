__author__ = 'cocoon'
"""
    adb tools collection

        /adb


        # collection operations
        /adb/-/devices :  list of connected android devices


"""

from flask import request
from flask import json
from droydserver.lib.facets.rest_collections import CollectionWithOperationApi

from droydserver.mobiles import ADB

#from droydserver.lib.facets  import InvalidUsage, ApplicationError



api = ADB



class AdbTools(CollectionWithOperationApi):
    """

    """
    def agent_to_user_session(self,agent_name):
        """
            convert agent_name to username and session Alice-123  (Alice,123)
        """
        return agent_name.split('-')


    def operation(self,**kwargs):
        """

            dispatch operation

            /adb/<item>/<operation>


            eg :
                adb/-/devices

        :param operation str: operation name
        :param item_id str: user name
        :return:
        """
        # it is an operation
        #try:

        operation = kwargs['operation']

        r = request

        try:
            data = r.json or None
        except:
            data = None

        agent = kwargs['item']

        # ignore agent , agent should be -   ( adb/-/<operation>

        # find operation method eg ADB.devices
        method = getattr(api,operation)

        # call collection method
        try:
            if data:
                rs = method(**data)
            else:
                rs = method()
        except:
            raise

        result = dict( result= rs , text = "result: %s , operation: %s " % (str(rs) , operation))
        #return "result: %s , operation: %s " % (str(rs) , operation)
        return json.dumps(result)

        # except:
        #
        #     raise ApplicationError('error on operation: %s' % operation )
