__author__ = 'cocoon'


import os
import subprocess
#import uiautomatorlibrary
import uuid

from uiautomatorlibrary.Mobile import Mobile as BaseMobile
from uiautomatorlibrary.Mobile import ADB as BaseADB



cmd_adb_devices = '/usr/local/bin/adb'


def adb_devices():
    """
        list connected android devices

        return a list of array
            0: device_id
            1: status

        eg: [
                ['e7f54be6', 'device'],
                ['388897e5', 'device']
            ]


    """
    device_list=[]
    result = subprocess.Popen( "%s devices" % cmd_adb_devices, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    lst = result[0].split("\n")
    # skip first line
    lst.pop(0)
    for line in lst:
        if line:
            device_id,status = line.split('\t')
            device_list.append([device_id,status])
    return device_list




class Mobile(BaseMobile):
    """
        interface to a single mobile

    """
    def wait_update(self):
        """

        """
        return self.device.wait.update()

    def wait_idle(self):
        """

        """
        return self.device.wait.idle()

    def dump(self,filepath=None):
        """
            get the dumped window hierarchy content(unicode)
        """
        content= self.device.dump()
        print "dump:\n"
        print content
        print "---\n"
        return unicode(content)

    def flash(self,scale=1.0,quality=100):
        """
            return base64 screenshot

        :return:
        """
        self.device.screenshot("image.png",scale=scale,quality=quality)
        with open("image.png","rb") as fh:
            data = fh.read()
            encoded= data.encode("base64")
            os.unlink("image.png")
            print("flash:\n")
            print encoded
            print("----\n")
            return encoded


    def get_object_data(self, **selectors):
        """

            DEPRECATED use get_object instead

        Get data dictionary of the UI object with selectors *selectors*

        See `introduction` for details about identified UI object.

        Example:
        | ${main_layer} | Get object_data | className=android.widget.FrameLayout | index=0 | # Get main layer which class name is FrameLayout |
        """
        obj= self.device( **selectors)
        return obj.selector


    def scroll_to_vertical(self, scroll_class,**selectors):
        """
        Scroll(vertically) on the object found with classname=scroll_class: obj to specific UI object which has *selectors* attributes appears.

        Return

        Example:

        | ${obj_data}    | Get Object Data      | className==android.widget.ListView |              | # Get the list object     |
        | ${found} | Scroll to Vertical | scroll_class=${obj_data['className']} | text=WebView | # Scroll to text:WebView. |
        """
        # LT: new keyword
        scroll_obj= self.device(className=scroll_class)
        is_here= scroll_obj.scroll.vert.to(**selectors)
        return  is_here

    def scroll_to_horizontal(self, scroll_class,**selectors):
        """
        Scroll(horizontally) on the object found with classname=scroll_class: obj to specific UI object which has *selectors* attributes appears.

        Return data of the target obj

        Example:

        | ${obj_data}    | Get Object Data      | className==android.widget.ListView |              | # Get the list object     |
        | ${target_data} | Scroll to Horizontal | scroll_class=${obj_data['className']} | text=WebView | # Scroll to text:WebView. |
        """
        # LT: new keyword
        scroll_obj= self.device(className=scroll_class)
        is_here= scroll_obj.scroll.horiz.to(**selectors)
        return  is_here


    # d(className="android.app.ActionBar$Tab").child(text="Bluetooth")
    def click_tab_by_text(self,className, text):
        """
            select all objects with className  ( eg android.app.ActionBar$Tab )
            for each find a child with text= text
            if found : click on it

        """
        # d(className="android.app.ActionBar$Tab").child(text="Bluetooth")
        result=False
        tabs= self.device(className=className)
        for tab in tabs:
            # find child object
            obj= tab.child(text=text)
            if obj:
                result= obj.click()
                return True
        return result

    def click_linear_by_text(self,className, text):
        """
            select all objects with className  ( eg android.app.ActionBar$Tab )
            for each find a child with text= text
            if found : click on it

        """
        # android.widget.LinearLayout
        result=False
        tabs= self.device(className=className,clickable=True,scrollable=False)
        for tab in tabs:
            # find child object
            print "tab: %s" % str(tab.selector)
            obj= tab.child(text=text,className="android.widget.TextView")
            if obj:
                print "obj: %s " % str(obj.selector)
                result= obj.click()
                return True
        return result


    def snapshot(self,name=None,scale=1.0,quality=100):
        """
            take a screenshot  and save to  snapshot_<name>.png
            take a dump and save to snapshot_<name>.uix

            if no name specified take a random uuid

        """
        if not name:
            name=uuid.uuid4()
        png_name= "snapshot_%s.png" % name
        uix_name= "snapshot_%s.uix" % name
        # take the screenshot
        self.device.screenshot(png_name,scale,quality)
        # dump to uix
        self.device.dump(uix_name)
        #logger.info('\n<a href="%s">%s</a><br><img src="%s">' % (screenshot_path, st, screenshot_path), html=True)
        return True


class ADB(BaseADB):
    """

    """

    @classmethod
    def devices(cls):
        """

        :return: a list of connected devices  (device_id,status)
        """
        return adb_devices()