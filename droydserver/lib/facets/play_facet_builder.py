from mimetype_facets import FacetsBuilder , Item , Template


from mimetype_facets import FacetsScanner

sample_item = dict(

    name= "hello", role="client" , inbox="http://theclient/rendez-vous/inbox/123456", mode="passive"


)


def test_mimetype():
    """

    """
    m = FacetsBuilder.new()

    m.set("href","http://theserver/rendez-vous/server/subscribe/123456789")

    m.add_link('self',href= "http://theserver/rendez-vous/server/subscribe/123456789")


    item =Item.from_dict("http://myUri",sample_item)

    m.add_item(item)

    template_data= (
        ('user',"user name for the session"),
        ('client' , "client session url"),
        ('inbox','client inbox url'),
        ('outbox','client outbox url')
    )
    for name,prompt in template_data:
        m.add_template_data(name,prompt)


    template= Template.from_dict(sample_item)

    m.set_template(template)

    print m.content

    print m.to_json

    print m.pretty_print


    r = FacetsScanner.from_json(m.to_json)
    print r.has_error()
    is_ok = r.validate()

    items= r.items()



    return

def test_error():
    """

    """
    m = FacetsBuilder.new()

    m.set("href","http://localhost:5000/adb/*/devices")

    m.set_error(code='500',title="fail" , message="cannot create element")

    print m.content

    print m.to_json

    print m.pretty_print



    r = FacetsScanner.from_json(m.to_json)

    print r.has_error()

    is_ok = r.validate()

    items= r.items()



    return


if __name__ == '__main__':

    test_mimetype()
    test_error()
    print