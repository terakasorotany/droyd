__author__ = 'cocoon'

import sys
import traceback

#     exc_type, exc_value, tb = sys.exc_info()


from flask import jsonify

class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


class ApplicationError(Exception):
    status_code = 500

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code

        exc_type, exc_value, tb = sys.exc_info()
        self.payload={}
        self.payload['tb']= traceback.extract_tb(tb)
        self.payload['exc_value']= exc_value.message
        self.payload['exc_type']= str(exc_type)

        # update with custom payload
        payload = dict(payload or ())
        self.payload.update(payload)


    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv
