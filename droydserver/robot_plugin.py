__author__ = 'cocoon'
"""
    robot_framework plugin for droyd hub server

    need droydclient package


"""

from droydclient.robot_plugin import Pilot , RF_HUB_URL

global RF_HUB_URL



# import inspect
# import os
# from gateway import DroydHub


# # environment variable , None: local , else: the url of the Droyd Hub
# RF_HUB_URL = None


# class Pilot(object):

#     def __init__(self,*args,**kwargs):
#         """

#         :param args:
#         :param kwargs:
#         :return:
#         """
#         self._args = args
#         self._kwargs = kwargs

#         # check for environ 'RF_HUB_URL'
#         RF_HUB_URL = os.environ.get('RF_HUB_URL',None)
#         self.RF_HUB_URL = RF_HUB_URL


#         self._api = DroydHub(url= RF_HUB_URL)

#         # extract all methods of Mobile
#         self._keywords = []
#         for name,func in inspect.getmembers(self._api._mobile_factory, predicate=inspect.ismethod  ):
#             if not name.startswith('_'):
#                 self._keywords.append(name)
#         # add session keywords
#         self._keywords.extend(['open_session','close_session'])

#         #print "mobile keywords: %s" % self._keywords

#     def get_keyword_names(self):
#         return self._keywords



#     # dynamic access to api methods
#     def __getattr__(self, name):
#         if name not in ['excluded',]:
#             return getattr(self._api,name)
#         raise AttributeError("Non-existing attribute '%s'" % name)


# if __name__ == "__main__":


#     p = Pilot()


#     print