__author__ = 'cocoon'
"""

    native API to droyd hub

        open_session
        close_session

        adb_devices

        + all device operations ( click ,press_home ...)


"""

import sys
import time

from droydserver.utils.store import Store as EmbededStore


#
# from droydrunner.uihub import UiHub
#
#
# # default log_cb
# stdout_no_buf = True
#
# def out(msg,nl=True):
#     """
#         send message to stdout and flush
#     """
#     if nl:
#         # newline : print msg + nl
#         print msg
#     else:
#         # send without new line
#         print msg,
#
#     if stdout_no_buf:
#         sys.stdout.flush()
#
#
#
# class Store(EmbededStore):
#     """
#
#     """
#
#     def setup(self):
#         pass
#
#
# class PhoneSession():
#     """
#
#     """
#     def __init__(self,store,**accounts):
#         """
#         """
#         self._store = store
#         self.add_users(**accounts)
#
#
#     def add_users(self,**accounts):
#         """
#
#         """
#         for k,v in accounts.iteritems():
#             self._store.add_entity('main',k,v)
#
#
#     def close(self):
#         """
#         """
#
#
#
#
#
# class NativeClient(object):
#     """
#         Native Client to DroyRunner Phone application via api.server.droyd_relay
#
#     """
#
#     def __init__(self,log_cb=None):
#         """
#
#         :param url:
#         :return:
#         """
#         self.title = 'uiphone'
#
#
#         if log_cb is None:
#             # no gven log callbck use the module default out() function
#             self.out = out
#         else:
#             # use the given callback for output
#             self.out=log_cb
#
#         self._store = Store()
#
#
#         # the phone session
#         self._session = None
#
#
#
#         # the relay
#         #self.relay = DroydRelay()
#
#         self.hub = UiHub()
#         self.agents = {}
#
#
#
#     def __enter__(self):
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         """
#
#         :param exc_type:
#         :param exc_val:
#         :param exc_tb:
#         :return:
#         """
#         self._store.close()
#
#
#     def log(self,msg):
#         """
#
#         """
#         self.out("pilot: %s" %  msg)
#
#
#     def open_session(self,**accounts):
#         """
#
#             open a phone session:
#                 store accounts
#         :param accounts:
#         :return:
#         """
#         #if self._session:
#         #    return self._session
#         # save session data
#         self._session = PhoneSession(self._store,**accounts)
#
#
#         # request to server
#         #return  self.relay.open_session(**accounts)
#
#
#         for device_id , device_data in accounts.iteritems():
#
#             serial = device_id
#             alias  = device_data.get('alias',serial)
#
#             # create and launch agent
#             self.log("Start Agent: %s with role: %s" % (device_id,alias))
#
#
#             # create an agent
#             agent = self.hub.add_device(alias=alias ,serial=serial,applications=None)
#
#             # update internal indexes
#             self.agents[serial]= agent
#
#
#             # open phone app
#             self.log("open phone for user %s" % serial)
#
#             self.agents[device_id].phone.open()
#
#             #self.execute(alias,'phone.open')
#
#         return True
#
#
#     def close_session(self,result=0,error=0):
#         """
#             close current session
#
#         :param session_id:
#         :return:
#         """
#         #session_name = self._session.content['session']
#
#         #return self.relay.close_session()
#
#         time.sleep(1)
#         self.shutdown()
#
#
#         if result == 0:
#             self.log( "<*> Test " + self.title + " completed successfully")
#         else:
#             self.log("<*> Test " + self.title + " completed with errors " + str(error) )
#
#         del self.agents
#         self.agents={}
#
#         return result
#
#     def shutdown(self):
#         """
#
#         """
#         # Shutdown all instances
#         self.log("shutdown ...")
#         #time.sleep(2)
#         for agent in self.agents.values():
#             self.log("shutdown agent")
#             #agent.shutdown()
#             agent.close()
#         self.log("shutdown completed")
#         return





#from uiautomatorlibrary import Mobile

from mobiles import Mobile ,ADB


class NativeDroydHub():
    """

        a hub to manage Mobiles

    """
    def __init__(self, mobile_factory= Mobile,  *args , ** kwargs):
        """

        :return:
        """
        # a map of Mobile instance
        #mobile_factory = Mobile

        self._mobile_factory = mobile_factory
        self._mobiles = {}


    def open_session(self,*device_ids):
        """

        :param device_ids:
        :return:
        """
        # build each mobile
        for d in device_ids:
            mobile = self._mobile_factory()
            mobile.set_serial(d)
            self._mobiles[d] = mobile



    def close_session(self):
        """

        :return:
        """

    def adb_devices(self):
        """

            list of android connected devices

        :return:
        """
        return ADB.devices()


    def scan_devices(self):
        """

            list the connected devices and return info (list)

        :return:
            - nb (int) number of connected devices
            - device_names (list)  a list of connected device names
            - device_status (dict) the status of devices
            - tags  a list of names ( userA , userB ... to set tags )


        """
        device_list = ADB.devices()
        nb=0
        device_names= []
        device_status= []
        tags= []

        nb = len(device_list)
        if nb:
            for i in xrange(0,nb):
                device_names.append(device_list[i][0])
                device_status.append(device_list[i][1])
                tags.append('user%d' % i)
        return nb,device_names,device_status,tags


    # mapping for the Mobile method ( click , wait_for_exists , press_home ... )
    def __getattr__(self, item):
        """
            wrapper to Mobile methods

        :param item:
        :return:
        """
        #if not item in getattr(Mobile):
        #    # this is not a Mobile attribute
        #    raise ValueError('not a Mobile attribute:%s' % item)
        # call a function
        def wrapper(device_id,*args,**kwargs):
            """

            """
            function_name = item
            mobile = self._mobiles[device_id]
            # intercept timeout arguments if any
            if 'timeout' in kwargs:
                # convert timeout to int
                kwargs['timeout'] = int(kwargs['timeout'])
            try:
                function = getattr(mobile,function_name)
            except AttributeError,e:
                raise e
            return function(*args,**kwargs)
        return wrapper


    def _convert(self,function_name, key_arguments):
        """

        :param function_name: str
        :param key_arguments: dict
        :return: a dict with correct type
        """
        int_keys = ['timeout','steps','x', 'y','sx','sy','ex','ey']
        obj_keys = ['obj']
        for key in key_arguments.keys():
            if key in int_keys:
                key_arguments[key] = int(key_arguments[key])
            elif key in obj_keys:
                raise NotImplementedError('cant accept obj yet')
        return key_arguments


NativeClient=NativeDroydHub