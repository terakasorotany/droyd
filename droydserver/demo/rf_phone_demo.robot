# robot framework test

*** Settings ***
Documentation     demo for droydhub
    ...
    ...  export RF_HUB_URL=http://hub.com:5000
    ...  pybot -L trace -P ../ rf_native_hub.txt
    ...

Library      droydserver.robot_plugin.Pilot
Library      droydclient.robot_extension.DroydExtension
Library      Collections



Suite Setup     unit setup devices




*** Variables ***
#${Alice}=    e7f54be6
#${Bob}=    388897e5
#
#${Alice_number}=     0684820364
#${Bob_number}=       0640412593




# [u'0a9b2e63', u'device'], [u'95d03672', u'device']
${Alice}=   95d03672
${Bob}=    0a9b2e63

${Alice_number}=     0631339829
${Bob_number}=       0688935353



*** Keywords ***


#
#  adb macros
#
Macro adb devices
   [Documentation]     unlock screen if necessary

    ${devices}  Adb devices
    Log List    ${devices}
    return from Keyword     ${devices}


#
#   general device macros
#

Macro Unlock Screen
    [arguments]     ${user}
    [Documentation]     unlock screen if necessary

    Turn On Screen  ${user}
    # get current screen
    ${device_info}  Get Device Info     ${user}
    #Log Dictionary  ${device_info}
    ${package_name}     Get From Dictionary     ${device_info}  currentPackageName
    Log     ${package_name}
    # return if not keyguard screen
    Return From Keyword If    '${package_name}' != 'com.android.keyguard'
    # swipe to unlock device
    Swipe By Coordinates   ${user}     sx=270     sy=1440    ex=800     ey=1000    steps=50


Macro Home
    [arguments]     ${user}
    [Documentation]     return to home screen
    Macro Unlock Screen     ${user}
    Press Home   ${user}
    Press Home   ${user}

Macro Quick Launch Phone
    [arguments]     ${user}
    [Documentation]     launch phone application from home screen with hot key
    click   ${user}  className=android.widget.TextView     packageName=com.sec.android.app.launcher    text=Phone


Macro Settings airplane on
    [arguments]     ${user}
    [Documentation]     put device in airplane mode (if necessary)
    # goto quick settings
    Open Quick Settings     ${user}
    # click on airplane logo
    click   ${user}     packageName=com.android.systemui  className=android.widget.LinearLayout  index=15
    # wait for popup with text: Turn On Airplane mode
    ${pop_up}   Wait for Exists     ${user}    timeout=1   text=Turn on Airplane mode  packageName=com.android.systemui  className=android.widget.TextView  resourceId=android:id/alertTitle
    # click on OK
    run keyword if  ${pop_up} == True  click   ${user}  packageName=com.android.systemui  className=android.widget.Button  text=OK
    # click on CANCEL
    run keyword if  ${pop_up} == False  click   ${user}  packageName=com.android.systemui  className=android.widget.Button  text=Cancel



Macro Settings airplane off
    [arguments]     ${user}
    [Documentation]     switch off airplane mode (if necessary)
    # goto quick settings
    Open Quick Settings     ${user}
    # click on airplane logo
    click   ${user}     packageName=com.android.systemui  className=android.widget.LinearLayout  index=15
    # wait for popup with text: Airplane mode
    ${pop_up}   Wait for Exists     ${user}    timeout=1   text=Airplane mode  packageName=com.android.systemui  className=android.widget.TextView  resourceId=android:id/alertTitle
    # click on OK
    run keyword if  ${pop_up} == True  click   ${user}  packageName=com.android.systemui  className=android.widget.Button  text=OK
    # click on cancel (already off)
    run keyword if  ${pop_up} == False  click   ${user}  packageName=com.android.systemui  className=android.widget.Button  text=Cancel



#
#  telephony macros
#


Macro Phone Keypad
    [documentation]    select keypad in phone application
    [arguments]     ${user}
    #return self.device(className='android.app.ActionBar$Tab',packageName=self.package_name,index=index).click()
    click   ${user}     className=android.app.ActionBar$Tab   packageName=com.android.contacts   index=0

Macro Phone dial number
    [Documentation]     dial number when in phone.keypad
    [arguments]     ${user}     ${number}

    # long click on text zone
    long click   ${user}    resourceId=com.android.contacts:id/digits
    #  click delete button
    click   ${user}    resourceId=com.android.contacts:id/deleteButton
    # set text with number
    set text    ${user}   input_text=${number}   resourceId=com.android.contacts:id/digits
    # press dial button
    click   ${user}    resourceId=com.android.contacts:id/dialButton


Macro Phone dial ussd
    [Documentation]     dial ussd when in phone keypad ( dont use dialButton)
    [arguments]     ${user}     ${number}

    # long click on text zone
    long click   ${user}    resourceId=com.android.contacts:id/digits
    #  click delete button
    click   ${user}    resourceId=com.android.contacts:id/deleteButton
    # set text with number
    set text    ${user}   input_text=${number}   resourceId=com.android.contacts:id/digits






# Macro Phone is incoming call

#     [Documentation]     is incoming pop up present
#     [arguments]     ${user}

#     #exists   ${user}   resourceId=com.android.incallui:id/


Macro Phone wait incoming call

    [Documentation]     wait for an incoming call
    [arguments]     ${user}     ${timeout}=10000

    ${call}     Wait For Exists   ${user}   timeout=${timeout}  resourceId=com.android.incallui:id/popup_call_state
    Return From Keyword If    ${call} == True
    Fail    msg=no incoming call received

Macro Phone answer call

    [Documentation]     click answer button in callui popup
    [arguments]     ${user}

    click   ${user}     resourceId=com.android.incallui:id/popup_call_answer

Macro Phone reject call

    [Documentation]     click CANCEL button in callui popup
    [arguments]     ${user}

     click   ${user}     resourceId=com.android.incallui:id/popup_call_reject

Macro Phone hangup call

    [Documentation]     click Hangup in incallui
    [arguments]     ${user}

     click   ${user}     resourceId=com.android.incallui:id/endButton


macro extract device
    [Documentation]   extract a particuliar device from device list
    [arguments]  ${devices}  ${index}

    Log Many    ${devices}
    ${user}=   get from list  ${devices[${index}]}   0
    return from keyword  ${user}

#
#   units
#
unit setup devices
    [documentation]  get connected devices and set Alice and Bob

    # get connected devices
    @{devices}  Adb devices
    Log Many    @{devices}
    Log Many    ${devices}

    ${no_devices} =	Run Keyword And Return Status	Should be empty 	${devices}
    Run Keyword If	${no_devices} 	Fail  no connected devices

    #Should not be empty 	${devices}
    #Run keyword if test failed  log  "no connected devices"

    #${number_of_connected_devices}=	 Count Values In List	@{devices}
    ${number_of_connected_devices}=	 Get length 	${devices}


    #${result}=   scan devices
    ${number_of_connected_devices}   ${device_names}  ${device_status}  ${tags}  scan devices
    Run keyword if  ${number_of_connected_devices}==${0}  Fail  no connected devices

    Remove tags  user1

    # set Alice
    Set Suite Variable  ${Alice}  ${device_names[0]}

    # set Bob
    Run keyword if  ${number_of_connected_devices}==${2}  Set Suite Variable  ${Bob}  ${device_names[1]}
    Run keyword if  ${number_of_connected_devices}==${2}  set tags  user1


#    Log Many    @{devices[0]}
#    ${userA}=   get from list  ${devices[0]}   0
#    Set Suite Variable  ${Alice}  ${userA}
#
#
#    run keyword if  ${number_of_connected_devices}>${1}  ${Bob}=  get from list  ${devices[1]}   0
#    log  ${Bob}

#   ${pass}  ${result}   run keyword and ignore error  get from list  ${devices[1]}   0
#   log  ${pass}
#   log  ${result}
#   run keyword if  '${pass}' == True   Set Suite Variable  ${Bob}  ${result}

#    ${userB}=  macro extract device  @{devices}  1
#
#    Log Many    @{devices[1]}
#    ${userB}=   get from list  ${devices[1]}   0
#    Set Suite Variable  ${Bob}  ${userB}


    #Set Suite Variable  ${Alice}  0a9b2e63
    #Set Suite Variable  ${Bob}  95d03672



#    ${i}=   set variable  ${0}
#    ${elements}=   set variable   ${EMPTY}
#    :FOR    ${device}    IN    @{devices}
#            #set list value  ${elements}  ${i}  ${devices[${i}]}
#        log    ${device}
#        #${i}=  set variable  ${i}  +  ${1}






Unit airplane
    [arguments]     ${userA}    ${userB}

    Open Session     ${userA}    ${userB}


    Macro Settings airplane on  ${userA}
    Builtin.sleep   1
    Macro Settings airplane off  ${userA}

    Macro Settings airplane off  ${userB}
    Builtin.sleep   1
    # check that airplane off works if already off
    Macro Settings airplane off  ${userB}



    Close Session


Unit Simple Call
    [Documentation]     A call B,B answers,A hangup
    [arguments]     ${userA}    ${userB}    ${number}   ${sleep}=2

    Open Session    ${userA}    ${userB}

    Macro Home  ${userA}
    Macro Quick Launch Phone    ${userA}
    Macro Phone Keypad  ${userA}
    Macro Phone Dial Number     ${userA}    ${number}



    Macro Home  ${userB}
    Macro Quick Launch Phone    ${userB}
    Macro Phone wait incoming call  ${userB}
    Macro Phone Answer Call     ${userB}

    Builtin.Sleep   ${sleep}

    Macro Phone hangup call     ${userA}

    Press Home  ${userA}
    Press Home  ${userB}

    [teardown]  Close Session


Unit Overall test
    [Documentation]    some tests
    [arguments]     ${userA}    ${userB}  ${sleep}=2

    Open Session    ${userA}    ${userB}

    Macro Home  ${userA}
    Macro Quick Launch Phone    ${userA}
    Macro Phone Keypad  ${userA}

    Builtin.Sleep   ${sleep}


    Press Home  ${userA}
    Press Home  ${userB}

    [teardown]  Close Session


Unit test dump
    [Documentation]   dump home screen
    [arguments]     ${userA}

    Open Session    ${userA}

    Macro Home  ${userA}

    Builtin.Sleep   1


    ${dump}=  dump  ${userA}
    log   ${dump}

    ${data}=    ril_service_mode  ${dump}

    log many  ${dump}


    [teardown]  Close Session


Unit test ril
    [Documentation]   get ril data
    [arguments]     ${userA}

    Open Session    ${userA}

    Macro Home  ${userA}
    Macro Quick Launch Phone    ${userA}
    Macro Phone Keypad  ${userA}
    Macro Phone Dial ussd     ${userA}    *#0011#

    Builtin.Sleep   1

    ${dump}=  dump  ${userA}
    log   ${dump}

    ${data}=    ril_service_mode  ${dump}

    log many  ${data}


    ${img}=  flash  ${userA}

    log  ${img}


    Macro Home  ${userA}

    [teardown]  Close Session






unit test get text

    [Documentation]   dump home screen
    [arguments]     ${user}

    Open Session    ${user}

    Macro Home  ${user}

    Builtin.Sleep   1



    ${data}=  get object ${user}  className=android.widget.TextView     packageName=com.sec.android.app.launcher    text=Phone
    log Many  ${data}
    log  ${data['text']}

    [teardown]  Close Session




*** Test Cases ***


connected devices
    [Documentation]     list connected devices
    [tags]  base
    macro adb devices


# test ril
#     [tags]  base  user0
#     Unit test ril  ${Alice}


# test dump
#     [tags]  base  user0
#     Unit test dump  ${Alice}


# test get text
#     [tags]  base  user0

#     unit test get text  ${Alice}


# overall test
#     [Documentation]     some test
#     [tags]  base   user1
#     Unit Overall test  ${Alice}    ${Bob}

#For-Loop-In-Range
#    [tags]    base
#    :FOR    ${INDEX}    IN RANGE    1    3
#        Log    ${INDEX}
#        ${RANDOM_STRING}=    Generate Random String    ${INDEX}
#        Log    ${RANDOM_STRING}


#airplane mode
#    [Documentation]     play with airplane mode
#    [tags]  airplane
#    Unit airplane   ${Alice}    ${Bob}
#
simple_call
   [Documentation]     userA call userB , userB answers call , sleep 2 , userA hangups
   [tags]  phone   call
   Unit Simple Call    ${Alice}    ${Bob}  ${Bob_number}




