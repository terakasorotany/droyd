# overview
droydrunner is a set of tool based on robot framework for test automation on android devices

it can be used as client server solution with droydclient and droydserver

# Documentation
The documentation can be found in the source repository file: **doc/droydrunner.html**

# Droydrunner client

## install it

droydclient is a python library; so **python** and **pip** are needed

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/droyd.git
    cd droyd
    pip install -r requirements_client.txt
    python setup.py install

## use it

droydclient can be be used via robotframework, a sample is available in source  

    player/rf_phone_demo.robot

to launch it see the sample script ( adapt the url to droydrunner server url)

    player/rf_http_phone_demo.sh

# Droydrunner server

## install it

droydserver needs several libraries to work , so to ease the process a docker image is available

you need to install docker to be able to use it. [refer to](https://docs.docker.com/installation/)

then type the command to install the image

    docker pull cocoon/droyd:latest

## use it

to launch a droydserver  type

    sudo docker run -d --privileged -v /dev/bus/usb:/dev/bus/usb  --name droydhub -P cocoon/droyd droydserve -- host 0.0.0.0 --port 5000


to check droydserver is working open a browser on  http://<your_host>:5000 , the server shoud respond with a **hello there**

connect android devices on the usb ports of the server

in your browser open 
    http://<your_host>:5000/adb/*/devices 

you should get a json list of connected devices

