__author__ = 'cocoon'
"""

    define rules and implement tools to handle uiautomator events

    ( rules are described in interceptions.yaml )



"""

# cases=[
#
#     'press home': {
#         'description': "",
#         'context': {
#             'package': "com.sec.android.app.laucher",
#         },
#         'action': {
#             'cmd': "",
#
#         }
#
#     },
#
#
#
#
# ]

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


import os
import re

from droydfsm import CallFsm , DefaultFsm

from droydfsm import InspectFsm


current_dir= os.path.dirname(os.path.realpath(__file__))


class Resolver(object):
    """

        convert line event to action with interceptions.yml
    """
    def __init__(self,rules=None):
        """


        :param rulefilename:
        :return:
        """
        self._user_resolvers= {}


        self.set_rules(rules)

        # golval state of resolver
        self._state= {

            "users": {}

        }



    @classmethod
    def from_yaml(cls,filename=None):
        """


        :param filename:
        :return:
        """
        if not filename:
            filename = os.path.join(current_dir,"interceptions.yml")


        with open(filename,"rb") as stream:

            interceptions = load(stream, Loader=Loader)

            rules= interceptions['rules']

            obj= cls(rules)

            return obj


    def set_rules(self,rules):
        """

        :param rules:
        :return:
        """
        self.rules=rules


    def resolve(self,event_line,rules=None,trace=0,indent='    '):
        """
        :param context:
        :param rules:
        :param trace:
        :return:
        """
        if not rules:
            rules=self.rules

        context= event_line.context

        #return resolve(context,rules,trace)
        status = dict( scroll="" )
        lines=[]
        for rule in rules:
            if trace:
                print "try rule: %s (%s)" % (rule['name'],rule['description'])

            ok = True
            for key in rule['context'].keys():
                if key.endswith('Like'):
                    #
                    context_key = key[:len(key)-4]
                    # regex comparison
                    pattern= re.compile(rule['context'][key])
                    m = pattern.match(context[context_key])
                    if not m:
                        ok= False
                        if trace:
                            print "next rule"

                else:
                    # strict comparison
                    if context[key] != rule['context'][key]:
                        ok = False
                        if trace:
                            print "next rule ..."
                        break
            if ok:
                # we found a rule : apply it
                event_line.rule= rule

                for line in self.apply_rule(event_line,status ,trace,indent=indent):
                    lines.append( indent + line)

               # for line in self.apply_rule(context,rule,status ,trace,indent=indent):
               #      lines.append( indent + line)


                return lines
                #if lines:
                #    return "\n".join(lines)
                #else:
                #    return None

            else:
                # no rule found
                pass
        if trace:
            print "no rule found for this context: %s" % str(context)


    def apply_common_rule(self,context,rule,status,trace=1):
        """
            apply common rule from knowledge base

        :param context:
        :param rule:
        :param trace:
        :return:
        """
        lines=[]
        result=[]
        action= rule['action']
        # generic cmd case
        result.append(action['cmd'])
        #result.append("${user%d}" % context['user'])
        result.append("${%s}" % context['user'])
        for key in action['selector']:
            if key == 'textOrDescription':
                if not context.get('ContentDescription',None) or context['ContentDescription']=="null":
                    # description is null take text
                    result.append('text=%s' % context['Text'].strip("[]"))
                else:
                    # keep description
                    #result.append('content-desc=%s' % context['ContentDescription'])
                    result.append('description=%s' % context['ContentDescription'])
            elif key == 'Text':
                result.append('text=%s' % context['Text'].strip("[]"))
            elif key == "ClassName":
                result.append( "%s=%s" % ("className" , context[key]))
            elif key == "PackageName":
                result.append( "%s=%s" % ("packageName" , context[key]))
            else:
                prop=key[:]
                prop[0]=key[0].lower()
                result.append( "%s=%s" % (prop , context[key]))
        line= "  ".join(result)
        lines.append(line)


        # update state
        if rule.get('state',None):
            # we found a state to update
            for k ,v in rule['state'].iteritems():
                self.update_state(k,v,context['user'])



        # update status
        if status['scroll']:
            # we are inside a scroll section , should ends on Click, Long_click , WINDOW_CHANGE
            if context['EventType'] in status['exit_scroll_mode_on']:
                # exit scroll mode
                status['scroll']= ""
                status['exit_scroll_mode_on']=[]
                if trace:
                    print("exit scroll section")





        return lines


    def apply_rule(self,context,rule,status, trace=0,indent="    "):
        """

        :param context:
        :param rule:
        :return:
        """
        if trace:
            print "found rule: %s" % str(rule)


        lines=[]
        scroll_factor= 10

        line=[]
        result=[]
        action= rule['action']

        if action.get('cmd',None):
            # if rule has a cmd clause ( Click , Long Click , Scroll



            if action['cmd'] == 'Scroll':

                line= None

                # set scroll mode
                status['scroll']= context['ClassName']
                status['exit_from_scroll']=rule['action']['exit_scroll_mode_on']

                # # handle scroll
                # from_index= int(context['FromIndex'])
                # to_index= int(context['ToIndex'])
                # item_count= int(context['ItemCount'])
                #
                # if from_index < 0 or to_index < 0:
                #     # null scroll => skip
                #     pass
                # else:
                #     delta= to_index - from_index
                #     if delta > 0:
                #         # scroll down
                #         steps= scroll_factor * delta
                #         line= indent + "Scroll Forward Vertically  ${%s}  steps=%s  className=%s" %(
                #             context['user'],str(steps),context['ClassName'])
                #     elif delta <0:
                #         # scroll up
                #         steps= scroll_factor + delta
                #         line= indent + "Scroll Backward Vertically  ${%s}  steps=%s  className=%s" %(
                #             context['user'],str(steps),context['ClassName'])


            else:
                # common cmd
                lines.extend(self.apply_common_rule(context,rule,status,trace))

        elif action.get('notification',None):
            # it is a notification
            name= action['notification']
            data= action['data']

            line= "%s  ${%s}  %s"  % ( name, context['user'], data)
            lines.append(line)

        else :
            # action is unkwon
            raise ValueError("invalid action: %s" % action)

        if trace:
            print lines

        return lines


    def update_state(self,key,value,user):
        """

        :param new_state:
        :param user:
        :return:
        """
        if not self._state.has_key(user):
            self._state[user]={}
        self._state[user][key]=value


# #interceptions= None
#
# with open(os.path.join(current_dir,"interceptions.yml"),"rb") as stream:
#
#     interceptions = load(stream, Loader=Loader)
#
#
# rules= interceptions['rules']


class UserResolver():
    """
        a set of fsm for a given user

            by default implements 2 fsm per user

            CallFsm and DEfaultFsm


    """
    def __init__(self,automate_classes=None):
        """

            automates: an array of fsm classes for the user

        """
        if automate_classes is not None:
            # set the given automates
            self._automate_classes= automate_classes
        else:
            # set defaults automates
            self._automate_classes=[CallFsm,DefaultFsm]


        # initialize the fsm(s)
        self._fsm= []
        for i, f in enumerate(self._automate_classes):
            # instantiate fsm and add it to fsm array
            self._fsm.append( self._automate_classes[i]() )
            # set out call back
            self._out=[]
            # define call back
            def out(msg):
                """
                    redefines the out function
                """
                self._out.append(msg)

            self._fsm[i].set_out(out)

        return


    def resolve(self,event,trace=0,indent='    '):
        """

        :param context:
        :param rules:
        :param trace:
        :param indent:
        :return:
        """
        # dispatch to each fsm in order
        out=[]
        for fsm in self._fsm:

            event_name= event.rule['name']

            self._out=[]
            r = fsm.trigger(event_name,event=event,trace=trace,indent=indent)
            out=self._out[:]

            # r == -1 : not treated
            # r == 0  : treated ok
            # r == n  : treated with errors

            if r >= 0 :
                # treated
                break

            # not treated, next fsm
            continue

        return out


class FsmResolver(Resolver):
    """
        a resolver based on a set of FSM  (Finite State Machine )

        generate a replay script

    """

    # a dictionary of UserResolver instances
    #_user_resolvers= {}


    _automates= [CallFsm,DefaultFsm]

    def apply_rule(self,event,status, trace=0,indent="    "):
        """

            dispatch rule to concerned user

        :param context:
        :param rule:
        :return:
        """
        context=event.context
        rule= event.rule


        if trace:
            print "found rule: %s" % str(rule)

        user= context['user']
        #event_name= rule['name']

        # test if there is already a userFsm created for this user
        if not self._user_resolvers.has_key(user):
            # create a user fsm for this user
            self._user_resolvers[user]= UserResolver(automate_classes=self._automates)

        # dispatch to user fsm
        r = self._user_resolvers[user].resolve(event,trace=0,indent='    ')


        return r


###   inspectors

class FsmInspector(FsmResolver):
    """
        a resolver based on a set of FSM  (Finite State Machine )

        generate an inspect script

    """

    _automates = [ InspectFsm ,]




if __name__=="__main__":





    # tests a list of test tuples ( context , result)
    tests= [
        (
        dict(EventType='TYPE_VIEW_CLICKED', PackageName="com.android.incallui", ClassName="android.widget.Button", Text="[End call]" ,user='user0'),
        "    Click  ${user0}  className=android.widget.Button  text=End call"
        ),

        (
        dict(EventType='TYPE_VIEW_CLICKED', PackageName="com.android.contacts", ClassName="android.widget.ImageButton", Text="[Call]" ,user='user0'),
        "    Click  ${user0}  className=android.widget.ImageButton  text=Call"
        ),


       (
         dict(EventType='TYPE_NOTIFICATION_STATE_CHANGED', PackageName='com.android.incallui',user='user0',ClassName="android.app.Notification",ParcelableDataLike="Notification(contentView=com.android.incallui/??)"),
         "    Notification  ${user0}  .*contentView=com.android.incallui/.*"

        ),

       (
         dict(EventType='TYPE_WINDOW_CONTENT_CHANGED', PackageName='com.android.incallui',user='user0',ClassName="android.widget.TextView",ContentDescription="Call ended."),
         "    Receive Call Ended  ${user0}"

        ),
        (
         dict(EventType='TYPE_WINDOW_STATE_CHANGED', PackageName='com.android.incallui',user='user0',ClassName="com.android.incallui.InCallActivity",TextLike='[Incoming call, + 3 3 6 8 8 9 3 5 3 5 3, France]'),
         "    Macro Phone wait incoming call  ${user0}"

        ),
        (
        dict(EventType='TYPE_VIEW_CLICKED', PackageName="*", ClassName="*",ContentDescription="*", Text="*" ,user='user0'),
        "    Click  ${user0}  className=*  description=*"
        ),(
        dict(EventType='TYPE_VIEW_CLICKED', PackageName="*", ClassName="*",ContentDescription="", Text="*" ,user='user0'),
        "    Click  ${user0}  className=*  text=*"
        ),(
        dict(EventType='TYPE_VIEW_LONG_CLICKED', PackageName="*", ClassName="*",ContentDescription="*", Text="*" ,user='user0'),
        "    Long Click  ${user0}  className=*  description=*"
        ),(
        dict(EventType='TYPE_VIEW_LONG_CLICKED', PackageName="*", ClassName="android.widget.EditText",ContentDescription="*", Text="*" ,user='user0'),
        "    Long Click  ${user0}  className=android.widget.EditText  packageName=*"
        ),(
        dict(EventType='TYPE_WINDOW_STATE_CHANGED', PackageName="*", ClassName="*",ContentDescription="*", Text="*" ,user='user0'),
        "    Wait Update  ${user0}"
        ),(
        dict(EventType='TYPE_WINDOW_STATE_CHANGED', PackageName="com.sec.android.app.launcher", ClassName="*",ContentDescription="*", Text="[Home screen]" ,user='user0'),
        "    Press Home  ${user0}"
        ),(
        dict(EventType='TYPE_VIEW_SCROLLED', PackageName="*", ClassName="*",ContentDescription="*", Text="" ,
             user='user0' ,FromIndex=0, ToIndex=9, ItemCount=15  ),None
        #"    Scroll Forward Vertically  ${user0}  steps=90  className=android.widget.ScrollView"
        ),
    ]





    def test_rules():
        """

        :param rules:
        :return:
        """


        trace= 1

        resolver= Resolver.from_yaml()


        for context,result in tests:
            command= resolver.resolve(context,trace=1)
            if command:
                assert (command[0]==result)
            else:
                assert(result==None)


    def test_UserResolver():
        """

        :return:
        """
        trace= 1

        resolver= FsmResolver.from_yaml()

        # simulate events
        event_indices= [1,2,3,  5]
        scenario=[]
        for i in event_indices:
            scenario.append(tests[i])


        for context,result in scenario:
            command= resolver.resolve(context,trace=1)
            continue



        return

    def test_DefaultFsm():
        """

        :return:
        """
        trace= 1

        resolver= FsmResolver.from_yaml()

        # simulate events
        event_indices= [5,6,7,8,9,10,11]
        scenario=[]
        for i in event_indices:
            scenario.append(tests[i])


        for context,result in scenario:
            command= resolver.resolve(context,trace=1)
            if command:
                assert (command[0]==result)
            print ">>>>> %s" % command
            continue


        return







    test_DefaultFsm()

    test_UserResolver()
    test_rules()








