# -*- coding: utf-8 -*-

__author__ = 'cocoon'


import re
import copy

from interceptions import Resolver

#line_sample = "06-19 10:22:06.875 EventType: TYPE_VIEW_CLICKED',' EventTime: 51397034; PackageName: android; MovementGranularity: 0; Action: 0 [ ClassName: android.widget.EditText; Text: []; ContentDescription: null; ItemCount: -1; CurrentItemIndex: -1; IsEnabled: true; IsPassword: true; IsChecked: false; IsFullScreen: false; Scrollable: false; BeforeText: null; FromIndex: -1; ToIndex: -1; ScrollX: -1; ScrollY: -1; MaxScrollX: -1; MaxScrollY: -1; AddedCount: -1; RemovedCount: -1; ParcelableData: null ]; recordCount: 0"


# sample="""\
# 06-19 10:22:03.726 EventType: TYPE_WINDOW_STATE_CHANGED; EventTime: 51393846; PackageName: android; MovementGranularity: 0; Action: 0 [ ClassName: android.widget.FrameLayout; Text: [Add widget, Thu, 19 Jun, 10, :22, Thu, June 19, Charged ÔÇö message]; ContentDescription: null; ItemCount: -1; CurrentItemIndex: -1; IsEnabled: true; IsPassword: false; IsChecked: false; IsFullScreen: false; Scrollable: false; BeforeText: null; FromIndex: -1; ToIndex: -1; ScrollX: -1; ScrollY: -1; MaxScrollX: -1; MaxScrollY: -1; AddedCount: -1; RemovedCount: -1; ParcelableData: null ]; recordCount: 0
# 06-19 10:22:04.660 EventType: TYPE_VIEW_TEXT_SELECTION_CHANGED; EventTime: 51394818; PackageName: android; MovementGranularity: 0; Action: 0 [ ClassName: android.widget.EditText; Text: []; ContentDescription: null; ItemCount: 0; CurrentItemIndex: -1; IsEnabled: true; IsPassword: true; IsChecked: false; IsFullScreen: false; Scrollable: false; BeforeText: null; FromIndex: 0; ToIndex: 0; ScrollX: -1; ScrollY: -1; MaxScrollX: -1; MaxScrollY: -1; AddedCount: -1; RemovedCount: -1;ParcelableData: null ]; recordCount: 0
# 06-19 10:22:04.798 EventType: TYPE_WINDOW_CONTENT_CHANGED; EventTime: 51394957; PackageName: android; MovementGranularity: 0; Action: 0 [ ClassName: android.widget.FrameLayout; Text: []; ContentDescription: null; ItemCount: -1; CurrentItemIndex: -1; IsEnabled: true; IsPassword: false; IsChecked: false; IsFullScreen: false; Scrollable: false; BeforeText: null; FromIndex: -1; ToIndex: -1; ScrollX: -1; ScrollY: -1; MaxScrollX: -1; MaxScrollY: -1; AddedCount: -1; RemovedCount: -1; ParcelableData: null ]; recordCount: 0
# 06-19 10:22:06.875 EventType: TYPE_VIEW_CLICKED; EventTime: 51397034; PackageName: android; MovementGranularity: 0; Action: 0 [ ClassName: android.widget.EditText; Text: []; ContentDescription: null; ItemCount: -1; CurrentItemIndex: -1; IsEnabled: true; IsPassword: true; IsChecked: false; IsFullScreen: false; Scrollable: false; BeforeText: null; FromIndex: -1; ToIndex: -1; ScrollX: -1; ScrollY: -1; MaxScrollX: -1; MaxScrollY: -1; AddedCount: -1; RemovedCount: -1; ParcelableData: null ]; recordCount: 0"""


event_ref= "http://developer.android.com/reference/android/view/accessibility/AccessibilityEvent.html"

keep_events=['TYPE_VIEW_CLICKED','TYPE_VIEW_LONG_CLICKED','TYPE_VIEW_TEXT_CHANGED']

action_args= [ 'ClassName','Text','ContentDescription','ItemCount','CurrentItemIndex','IsEnabled','IsPassword',
               'IsChecked','IsFullScreen','Scrollable','BeforeText','FromIndex','ToIndex','ScrollX','ScrollY',
               'MaxScrollX','MaxScrollY','AddedCount','RemovedCount','ParcelableData']


boolean_type= ['IsEnabled','IsPassword','IsChecked','IsFullScreen','Scrollable' ]
integer_type= [ 'EventTime', 'FromIndex', 'ToIndex', 'ScrollX', 'ScrollY', 'MaxScrollX', 'MaxScrollY', 'AddedCount',
                'RemovedCount', 'recordCount','MovementGranularity']
array_type= []


translations = [
    (r'click   ([^s]+)  className=android.widget.TextView  description=Phone', "Macro Quick Launch Phone" ),

]

class EventLine(object):
    """

        parser for a line of event

    """

    rline=re.compile(r'(.*?):(([0-9\s]*?\[.*\]\;)|(.*?\;))')
    rkey_value=re.compile(r'([a-zA-Z0-9]+):(.*?)\;')

    # patern for timestamp   eg 02-19 14:01:46.311
    timestamp_pattern= re.compile(r'\d\d\-\d\d \d\d\:\d\d\:\d\d\.\d\d\d$')

    def treat_action(self,action_line):
        """

        :param action_line: str  eg:  0 [ ClassName: android.widget.TextView; Text: [Phone]; ContentDescription: Phone; ...];
        :return: a dict
        """
        action={}
        for m in self.rkey_value.finditer(action_line):
            result= m.groups(1)
            key= result[0].strip()
            value= result[1].strip()
            if not value.startswith('['):
                value= value.strip(" ]")
            action[key]=value
            #action[result[0]]= result[1].strip(" ")
        return action


    def treat_line(self,event_line):
        """

        transform a uiautomator event to data structure

        :param event_line:
        :return: a dict
        """
        data= {}

        data['_timestamp']= event_line[0:18]
        # check this is a timestamp:
        m = self.timestamp_pattern.match(data['_timestamp'])
        if not m:
            # this not a time stamp
            return None



        events= event_line[19:]

        events = events + ';'


        for m in self.rline.finditer(events):

            result= m.groups(1)

            #print "|%s| |%s|" % (result[0],result[1])
            key= result[0].strip()
            value= result[1]

            if key != 'Action':
                value= value.strip(" ;")

            data[key]= value

        data['Action']= self.treat_action(data['Action'])

            #args[result[0].strip()]= result[1]
        return data



    def __init__(self,line,user='user'):
        """


        :param line:
        :return:
        """
        # self.args={}
        # self.keys = []
        # self.values=[]
        #
        # self.args['user'] = user

        line_data= self.treat_line(line)
        if line_data is None:
            # an invalid line data
            self.data={}
            self.data['valid']= False
            self.data['line']= line.strip(' \r\n')
        else:
            # a valid event line: starts with a timestamp
            self.data=line_data
            self.data['valid'] = True
        self.data['_user']= user

    def __repr__(self):
        """

        :return:
        """
        return "<%s>(%s):class=%s,text=%s,desc=%s" % (
            self.get('EventType'),self.get('_timestamp'),self.get_action('ClassName'),self.get_action('Text'),self.get_action('ContentDescription'))


    def is_valid(self):
        """

        :return:
        """
        return self.data['valid']

    def get(self,name):
        """
            retrieve the value of an argument

        :param name:
        :return:
        """
        return self.data[name]

    def get_action(self,param):
        """
            return an action parameter

        """
        return self.data['Action'][param]

    @property
    def event_time(self):
        #return int(self.data['EventTime'])
        return self.data['_timestamp']


    @property
    def context(self):
        """
            return a context
        """
        try:
            return self.__context
        except AttributeError:
            # compute context
            result={}
            for key in ['EventType','EventTime','PackageName','MovementGranularity','recordCount']:
                try:
                    result[key]=self.data[key]
                except KeyError:
                    pass
            #for key in ['ClassName','Text','ContentDescription','']:

            for key in [
                'ClassName','Text','Messages','ContentDescription','ItemCount','CurrentItemIndex','IsEnabled',
                'IsPassword','IsChecked','IsFullScreen','Scrollable','BeforeText','FromIndex','ToIndex','ScrollX',
                'ScrollY','MaxScrollX','MaxScrollY','AddedCount','RemovedCount','ParcelableData','nulrecordCount'
                ]:
                try:
                    result[key]=self.data['Action'][key]
                except KeyError:
                    pass
            self.__context= result

        return self.__context

    def parcelable(self,parcelabledata):
        """

            traet a field like  Notification(key=value, ...)

        """
        result={}

        if parcelabledata != 'null':

            pattern= re.compile(r'([^(]+)\((.*?)\)')
            m = pattern.match(parcelabledata)
            if m:
                kind= m.group(1)
                properties=m.group(2)
                result['_kind']= kind
                for e in properties.split():
                    k, v = e.split('=')
                    result[k.strip()]=v
        else:
            result['_kind']= 'null'

        return result



class EventFlow(object):
    """


    """
    def __init__(self,filename):
        """

        :param filename:
        :return:
        """
        self.filename= filename

        # an array of EventLine instance
        self.lines=[]


        self.resolver= Resolver.from_yaml()


    def iter_line(self,):
        """


        :return:
        """
        with open(self.filename,"r") as fh:
            for l in fh:
                yield  EventLine(l)
                continue


    def generate_header(self,title,documentation="...",arguments="${user}"):
        """

        :param title: str
        =param: documentation
        :param users:
        :return:
        """
        v=[]
        v.append("%s" % title)
        v.append("    [arguments]  %s" % arguments)
        v.append("    [Documentation]    %s" % documentation)
        v.append("")
        return v

    def print_header(self,title,documentation="...",arguments="${user}"):

        print "\n".join(self.generate_header(title,documentation,arguments))


    def generate_line(self,event_line, user="user"):
        """
            generate line with knowledge base

        :param event_line: an instance of EventLine
        :param user:
        :return:
        """
        #context= event_line.context
        #context['user']= user
        #line= self.resolver.resolve(context)


        event_line.context['user']= user
        line= self.resolver.resolve(event_line)


        return line

    def generate_line2(self,event_line, user="user"):
        """

            return a line of script code
        :param event_line:  EventLine instance
        :return:
        """
        line=event_line

        event_type= line.get('EventType')

        package= line.get('PackageName')
        className = line.get_action('ClassName')
        text= line.get_action('Text')
        desc= line.get_action('ContentDescription')

        output= ""

        if  event_type == 'TYPE_VIEW_CLICKED' or event_type == 'TYPE_VIEW_LONG_CLICKED':
            # handle click and long click action

            if event_type == 'TYPE_VIEW_CLICKED':
                action= 'click'
            else:
                action= 'long click'

            if desc and desc != 'null':
                slug_type= 'description'
                slug= desc
            elif text:
                slug=text.strip('[]')
                slug_type= 'text'
            else:
                raise ValueError("desc and text are null")

            # generate click action
            if action == 'click':
                output= "    %s   ${%s}  className=%s  %s=%s" % (action,user,className,slug_type,slug)
            else:
                output= "    %s   ${%s}  className=%s" % (action,user,className)

        elif event_type == 'TYPE_WINDOW_STATE_CHANGED':
            # window change , wait for it
            #${screen_change}   Wait for Exists     ${user}    timeout=1   text=Airplane mode  packageName=com.android.systemui  className=android.widget.TextView
            output= "    wait update  ${%s}  " %  user
            output+= "\n    # %s package is now: %s , class=%s" % (user, package, className)
        else:
            # event type is not clickable
            return

        return output


    def print_line(self,event_line, user="user"):
       print "\n".join(self.generate_line(event_line,user))








class Scripter(object):
    """
        create the demo script
    """
    main_pattern="""\
*** Settings ***
Documentation   robot framework replay of a droydscripter script
    ...

Library 	droydclient.robot_plugin.Pilot

Test Teardown  close session

*** Variables ***
%(variables)s

*** Keywords ***

%(macros)s

%(keywords)s

*** Test Cases ***

replay
  open session  %(users)s

  # setup
%(setup)s

  # run the generated test
  %(title)s  %(users)s

   # teardown
%(teardown)s


"""


    macro_pattern = """\
#
#  telephony macros
#

Macro Phone Keypad
    [documentation]    select keypad in phone application
    [arguments]     ${user}
    click   ${user}     className=android.app.ActionBar$Tab   packageName=com.android.contacts   index=0

Macro Phone dial number
    [Documentation]     dial number when in phone.keypad
    [arguments]     ${user}     ${number}

    # long click on text zone
    long click   ${user}    resourceId=com.android.contacts:id/digits
    #  click delete button
    click   ${user}    resourceId=com.android.contacts:id/deleteButton
    # set text with number
    set text    ${user}   input_text=${number}   resourceId=com.android.contacts:id/digits
    # press dial button
    click   ${user}    resourceId=com.android.contacts:id/dialButton

Macro Phone wait incoming call

    [Documentation]     wait for an incoming call
    [arguments]     ${user}     ${timeout}=10000

    ${call}     Wait For Exists   ${user}   timeout=${timeout}  resourceId=com.android.incallui:id/popup_call_state
    Return From Keyword If    ${call} == True
    Fail    msg=no incoming call received

Macro Phone answer call

    [Documentation]     click answer button in callui popup
    [arguments]     ${user}
    click   ${user}     resourceId=com.android.incallui:id/popup_call_answer

Macro Phone reject call

    [Documentation]     click CANCEL button in callui popup
    [arguments]     ${user}
    click   ${user}     resourceId=com.android.incallui:id/popup_call_reject

Macro Phone hangup call

    [Documentation]     click Hangup in incallui
    [arguments]     ${user}
    click   ${user}     resourceId=com.android.incallui:id/endButton

"""


    def __init__(self,code,title='macro generated script',devices=None):
        """

        :return:
        """
        self.code=code
        self.title= title
        self.devices= devices



        self.default= dict(
            variables="", keywords="!!! NO KEYWORDS !!!" ,users="  ${user}" ,title=self.title, macros=self.macro_pattern,
            setup="",teardown=""
            )


    def variables(self,devices=None):
        """

        generate variable section

            ${user0}= ???
            ${user1}= ???

        :param devices:
        :return:
        """
        v=[]
        for index,device in enumerate(devices):
            v.append("${user%d}=  %s" % (index +1,devices[index]))
        return v

    def setup(self,devices):
        """

        generate setup section

            Press Home  ${user0}
            Press Home  ${user1}

        :param devices:
        :return:
        """
        v=[]
        for index,device in enumerate(devices):
            v.append("  Press Home  ${user%d}" % (index +1) )
        return v

    def users(self,devices):
        """

        :param devices:
        :return:
        """
        v=[]
        for index,device in enumerate(devices):
            v.append("  ${user%d}" % (index+1 ) )
        return v


    def get_code(self,code):
        """


        :param code:
        :return:
        """
        return self.code


    def out(self,filename=None):
        """

        :return:
        """

        vars= copy.copy(self.default)
        vars['users']= "".join(self.users(self.devices))
        vars['variables'] = "\n".join(self.variables(self.devices))
        vars['setup']= '\n'.join(self.setup(self.devices))
        vars['teardown']= vars['setup']
        vars['macros']= self.macro_pattern
        vars['keywords']= self.get_code(self.code)

        script= self.main_pattern % vars
        print script

        if filename:
            with open(filename,"w") as fh:
                fh.write(script)



if __name__=="__main__":



    sample= "./samples/droydscripter_2.log"


    code="""\
macro generated code
    nop
"""


    def test_a_log_file():


        flow= EventFlow(sample)
        for line in flow.iter_line():
            #print line

            # catch parcelableData
            pdata= line.data['Action']['ParcelableData']
            if pdata:
                if pdata != 'null':
                    print pdata

                    data= line.parcelable(pdata)
                    continue

            continue

        return



    def test_1():

        script =Scripter(code,"macro generated code",devices= ['aaaab','bbbc'])

        script.out()


        # file_sample= "samples/events_1.csv"
        #
        #
        # f= EventFlow(file_sample)
        #
        # for line  in f.iter_line():
        #     print line
        #     continue



        print

    # starts here
    test_a_log_file()
    print