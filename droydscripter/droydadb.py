__author__ = 'cocoon'

import psutil

import os
import subprocess

def get_process(filter=None,verbose=True):
    """


    :param filter:
    :return:
    """
    adb_filter= "/adb -s *"

    filter=adb_filter

    # get process
    # if filter:
    #     ps = subprocess.check_output('ps aux | grep "%s"' % filter,shell=True)
    # else:

    ps= subprocess.check_output('ps aux' ,shell=True)

    # treat process
    processes = ps.split('\n')
    # this specifies the number of splits, so the splitted lines
    # will have (nfields+1) elements
    nfields = len(processes[0].split()) -1
    retval = []
    for row in processes[1:]:

        proc_data= row.split(None, nfields)
        if len(proc_data) == 0:
            continue

        pid= int(proc_data[1])
        command= proc_data[-1]

        if filter:
            if not filter in command:
                continue

        if verbose:
            ps=proc_data
            print "USER              PID       %CPU        %MEM       VSZ        RSS        TTY       STAT      START TIME      COMMAND"
            print "%-10.10s %10.10s %10.10s %10.10s %10.10s %10.10s %10.10s %10.10s %10.10s  %s" % (ps[0], ps[1], ps[2], ps[3], ps[4], ps[5], ps[6], ps[7], ps[8], ps[9])



        # if filter : dont add the grep command itself
        if filter:
            pass


        retval.append(proc_data)


    return retval



def getProcessData():
    ps = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE).communicate()[0]
    processes = ps.split('\n')
    # this specifies the number of splits, so the splitted lines
    # will have (nfields+1) elements
    nfields = len(processes[0].split()) - 1
    retval = []
    for row in processes[1:]:
        retval.append(row.split(None, nfields))
    return retval

# wantpid = int(contents[0])
# pstats = getProcessData()
# for ps in pstats:
#     if (not len(ps) >= 1): continue
#     if (int(ps[1]) == wantpid):
#         print "process data:"
#         print "USER              PID       %CPU        %MEM       VSZ        RSS        TTY       STAT      START TIME      COMMAND"
#         print "%-10.10s %10.10s %10.10s %10.10s %10.10s %10.10s %10.10s %10.10s %10.10s  %s" % (ps[0], ps[1], ps[2], ps[3], ps[4], ps[5], ps[6], ps[7], ps[8], ps[9])





def get_ab_shell():

    result = subprocess.check_output('ps -ef | grep "/adb -s *"',shell=True)

    print result


def get_adb_shell_process():
    """


    :return:
    """
    adb_process=[]
    for p in psutil.process_iter():
        if p.name() == 'adb':
            adb_process.append(p)


    for p in adb_process:
        print p
        continue


    return adb_process


if __name__=="__main__":

    adb= get_process()

    #adb = get_adb_shell_process()

    print adb