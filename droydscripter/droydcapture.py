__author__ = 'cocoon'
"""

    a script to capture keystrokes on android devices



    adb -s 95d03672 shell uiautomator events > samples/adb_1.log &
    adb -s 0a9b2e63 shell uiautomator events > samples/adb_2.log &



"""
import sys
import os
import time
import subprocess
import signal

from droydwrite import EventFlowMulti

adb_devices_cmd= "adb_devices"

cmd= "exec /usr/local/bin/adb -s %s shell uiautomator events > %s_%s.log"


def trace(msg):
    print msg




def get_adb():
    """

    :return:
    """
    #adb= subprocess.check_output("sh which adb")
    adb="/usr/local/bin/adb"
    return adb

def get_connected_devices():
    """

    :return: list : list of connected devices
    """
    devices=[]

    adb= get_adb()

    raw_devices= subprocess.check_output("%s devices" % adb,shell=True)

    lines= raw_devices.split('\n')
    for line in lines[1:]:
        if line:
            device,status= line.split('\t')
            devices.append(device)


    print raw_devices
    return devices



def kill_adb_shell(device):
    """

    ps aux :
    cocoon          43809   0,0  0,0   604140   1560 s006  S     6:12     0:00.02 /usr/local/opt/android-sdk/platform-tools/adb -s 95d03672 shell uiautomator runtest bundle.jar uiautomator-stub.jar -c com.github.uiautomatorstub.Stub

    :param device:
    :return:
    """
    filter= "/adb -s %s shell uiautomator" % device
    ps = subprocess.check_output('ps aux | grep "%s"' % filter,shell=True)
    ps = ps.split('\n')
    if isinstance(ps,basestring):
        ps =[ps,]
    for p in ps:
        if ' grep' in p:
            continue
        if not p :
            continue
        data=p.split(None)
        pid= int(data[1])
        os.kill(pid, signal.SIGQUIT) #or signal.SIGKILL
    return

class DroydRecorder(object):
    """
        launch a adb uiautomator events process for each connected devices


    """
    def __init__(self,base_name='uiautomator_events',title='macro generated_script'):
        """

        :return:
        """
        self.base_name=base_name
        self.title= title
        self.devices=get_connected_devices()

        self.proc= {}

    def log(selfself,msg):
        trace(msg)

    def start_device(self,device,indice=None):

            self.proc[device]=None

            if not indice:

                indice= device

            fullcmd = cmd % (device,self.base_name,str(indice))
            shell = True
            trace("Popen " + fullcmd  )
            self.proc[device] = subprocess.Popen(fullcmd, shell=shell, bufsize=0, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                         universal_newlines=False)
            started = self.proc[device].poll()
            return started

    def wait_device(self,device):
        trace("wait")
        self.proc[device].communicate()

    def stop_device(self,device):
        """
        """
        self.proc[device].kill()
        if sys.hexversion >= 0x02060000:
            self.proc[device].terminate()
        else:
            self.wait_device(device)

    def is_device_running(self,device):
        """
            return the subprocess status

        """
        if self.proc[device] is None:
            #  proc has not been started
            return False
        status = self.proc[device].poll()
        if status == None:
            # process is still running
            return True
        else:
            # process has returned
            self._status = status
            return False


    def run(self):
        # start capture on devices


        # create the generator
        generator= EventFlowMulti(base_name=self.base_name,title=self.title,devices=self.devices)
        generator.clean_script()

        indice= 1
        for device in self.devices:
            started= self.start_device(device,indice)
            indice += 1


        # wait user action
        r= raw_input("press any key to resume")

        # shutdown devices
        self.shutdown()


        print "generate script skeleton ..."
        print
        generator.generate_script("%s_script.robot" % self.base_name)

    def kill_adb_devices_shell(self):
        """

        """
        for device in self.devices:
            kill_adb_shell(device)




    def shutdown(self):
        """

        """
        # Shutdown all instances
        self.log("shutdown ...")
        time.sleep(2)
        for device in self.devices:
               #self.proc.values():
            self.log("shutdown device: %s" % device)
            self.stop_device(device)
        self.log("shutdown completed")
        return




if __name__=="__main__":


    devices= get_connected_devices()
    print devices

    for device in devices:
        kill_adb_shell(device)

    p= DroydRecorder("samples/uiautomator_events")
    p.run()
