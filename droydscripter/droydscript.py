#!usr/bin/env python
# -*- coding: UTF-8 -*-
"""droydscript

    a tool to record keystrokes on android devices , generate droydrunner script, and replay it
    main commands:
       rec:  record a script
       play: play a generated script
       info: get info for connected devices and scripts
       gen:  regenerate script from a previous recording
       clear: kill adb shell for each device connected

Usage:
  droydscript rec  [--name=<name>]
  droydscript play [--name=<name>]
  droydscript gen  [--name=<name>]
  droydscript info
  droydscript clear

Options:
  -h --help     Show this screen.
  --version     Show version.
  --name=<name>   [default: droydscripter] base name to generate files (name_?.log , name_script.robot) .

"""
import sys
from docopt import docopt




version = '0.0.2'




def main():
    arguments = docopt(__doc__, version=version)

    if arguments['rec']:
        # record keystrokes on android devices
        print "recording mode , name=%s" % arguments['--name']

        from droydcapture import DroydRecorder

        p= DroydRecorder(arguments['--name'])
        p.kill_adb_devices_shell()
        p.run()

    elif arguments['info']:
        print("# show info")
        print("## connected devices")

        import glob
        from droydcapture import get_connected_devices
        get_connected_devices()

        print("## list scripts:")
        for filename in glob.glob("*_script.robot"):
            name= filename[:len(filename)-len("_script.robot")]
            print "* %s  (%s)" % (name,filename)

    elif arguments['clear']:
        print("# list devices")
        print("## connected devices")

        import glob
        from droydcapture import get_connected_devices ,kill_adb_shell
        devices= get_connected_devices()

        for device in devices:
            print("kill adb shell for device: %s" % device)
            kill_adb_shell(device)


    elif arguments['play']:
        print "play droydscript %s" % arguments['--name']

        import subprocess
        cmd= "pybot -L trace -P ../ %s_script.robot" %   arguments['--name']
        print "call %s" % cmd
        subprocess.call(cmd,shell=True)
        #export RF_HUB_URL=
        #pybot -L trace -P ../ generated_script.robot

    elif arguments['gen']:
        print "regenerate droydscript %s" % arguments['--name']

        from droydwrite import EventFlowMulti
        from droydcapture import get_connected_devices

        devices= get_connected_devices()

        g = EventFlowMulti(base_name=arguments['--name'],devices=devices)

        g.generate_script()

    else:
        print "others"

    return 0


if __name__ == '__main__':
    sys.exit(main())

