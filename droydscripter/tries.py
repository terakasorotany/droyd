__author__ = 'cocoon'


from droydcore import EventFlow



if __name__=="__main__":


    #file_sample= "samples/events_1.csv"
    #file_sample= "samples/adb_1.log"
    file_sample= "samples/uiautomator_events_1.log"


    f= EventFlow(file_sample)

    print "macro sample"
    print "    [arguments]  ${user}"
    print "    [Documentation]    ..."
    print


    # generate click actions
    for line  in f.iter_line():
        #print line

        event_type= line.get('EventType')

        package= line.get('PackageName')
        className = line.get_action('ClassName')
        text= line.get_action('Text')
        desc= line.get_action('ContentDescription')


        if  event_type == 'TYPE_VIEW_CLICKED' or event_type == 'TYPE_VIEW_LONG_CLICKED':
            # handle click and long click action

            if event_type == 'TYPE_VIEW_CLICKED':
                action= 'click'
            else:
                action= 'long click'

            if desc and desc != 'null':
                slug_type= 'description'
                slug= desc
            elif text:
                slug=text.strip('[]')
                slug_type= 'text'
            else:
                raise ValueError("desc and text are null")

            # generate click action
            print "    %s   ${user}  className=%s  %s=%s" % (action,className,slug_type,slug)

        elif event_type == 'TYPE_WINDOW_STATE_CHANGED':
            # window change , wait for it
            #${screen_change}   Wait for Exists     ${user}    timeout=1   text=Airplane mode  packageName=com.android.systemui  className=android.widget.TextView
            print "    wait update  ${user}  timeout=5"
            print "    # package is now: %s , class=%s" % (package, className)
        else:
            # event type is not clickable
            pass
        continue


print