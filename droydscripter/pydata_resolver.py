__author__ = 'cocoon'
"""

    JUST A TRY

    resolver

    given an event choose the action and selector to apply


    event has properties
        type
        className
        text
        desc
        package
        clickable ...


    action is a string click , long click ..
    selector has properties
      A_className
      A_text
      A_desc
      A_package



    examples:

      1) click
          event
            EventType: TYPE_VIEW_CLICKED
            PackageName: com.sec.android.app.launcher
            ClassName: android.widget.TextView
            ContentDescription: Phone
            Text: [Phone]

          generate action

          action= "click ${user}
          selector
             ClassName: android.widget.TextView
             ContentDescription: Phone

      2) long click
            event:
                EventType: TYPE_VIEW_LONG_CLICKED
                ClassName: android.widget.EditText
            action: long click ${user}
            selector:
                ClassName: android.widget.EditText




"""
from pyDatalog import pyDatalog


pyDatalog.create_terms('X,Y','Z')

pyDatalog.create_terms('has_action')

#inputs
pyDatalog.create_terms('EventType,ClassName,ContentDescription','TextOrDescription')

#outputs
pyDatalog.create_terms( 'Action,A_class,A_text,A_desc')


pyDatalog.create_terms( 'has_class,has_text_or_description')

def has_action(event_type):

    return Action[event_type]


def get_class(event_type):

    if has_class[event_type]:
        return ClassName

def get_text_or_description(event_type):

    if has_text_or_description[event_type]:
        return TextOrDescription


# def has_class(even_type):
#
#     if ClassName[event_type]:
#         return



# rules
Action['TYPE_VIEW_CLICKED']= "click ${user}"
has_class['TYPE_VIEW_CLICKED']= True
has_text_or_description['TYPE_VIEW_CLICKED']= True


#
Action['TYPE_VIEW_LONG_CLICKED']= "long click ${user}"
has_class['TYPE_VIEW_LONG_CLICKED']= True



# context

EventType='TYPE_VIEW_CLICKED'
ClassName='android.widget.TextView'
TextorDescription='Phone'

print(Action[EventType]==X)
print(get_class(EventType)==A_class)
print (get_text_or_description(EventType)==Z)

#print ( X ,Y, Z)


# EventType='TYPE_VIEW_LONG_CLICKED'
# ClassName='android.widget.EditText'
# TextorDescription='dummy'
#
# print(Action[EventType]==X)
# print(get_class(EventType)==A_class)
# print (get_text_or_description(EventType)==Z)

#print ( X ,Y, Z)

# print(Selector[EventType]==Y)

#+ has_action('TYPE_VIEW_CLICKED','click  ${user}')

#+ has_action('TYPE_VIEW_CLICKED','click  ${user}')




# def twice(a):
#     return a+a
#
# pyDatalog.create_terms('twice,X,Y')
# print((X==1) & (Y==twice(X)))


# def action(event_type):
#
#     return Action[event_type]
#
#
# pyDatalog.create_terms('EventType,ClassName,ContentDescription,Text,Action,A_class,A_text,A_desc')
#
#
# pyDatalog.create_terms('action','Selector','X','Y')
#
#
# # rules
# Action['TYPE_VIEW_CLICKED']= "click ${user}"
#
# Action['TYPE_VIEW_LONG_CLICKED']= "long click ${user}"
#
# Selector['TYPE_VIEW_CLICKED'] = "class text_or_desc"
# Selector['TYPE_VIEW_LONG_CLICKED'] = "class"
#
#
#
#
# EventType='TYPE_VIEW_CLICKED'
# print(Action[EventType]==X)
# print(Selector[EventType]==Y)
#
# print(action(EventType)==X)
#
# EventType='TYPE_VIEW_LONG_CLICKED'
#
# print(Action[EventType]==X)
# print(Selector[EventType]==Y)
#
# #print(Action['TYPE_VIEW_CLICKED']==Y)