from distutils.core import setup

setup(
    name='droyd',
    version='0.2.3',
    packages=[ 'droydclient'],
    url='',
    license='',
    author='cocoon',
    author_email='tordjman.laurent@gmail.com',
    description='tools for android test automation'
)
