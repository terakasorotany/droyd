__author__ = 'cocoon'


from fysom import Fysom, FysomError



CALL_STATES= ['NULL','CALLING','INCOMING','EARLY','CONNECTING','CONFIRMED','DISCONNECTED']


# a fsm for calling application
class CallFsm(Fysom):
    """

    """
    cfg= {
        'initial': 'null',
        'events': [
            {'name': 'call', 'src': '*', 'dst': 'CALLING'},
            {'name': 'hangup', 'src': 'CONFIRMED', 'dst': 'DISCONNECTED'},
            {'name': 'incoming', 'src': '*', 'dst': 'INCOMING'},
            {'name': 'call_in_progress', 'src': ['CALLING','INCOMING','CONFIRMED'], 'dst': 'CONFIRMED'} ,
            {'name': 'reset', 'src': '*', 'dst': 'NULL'},
            {'name': 'call_ended', 'src': 'CONFIRMED', 'dst': 'NULL'},
            {'name': 'wild', 'src': '*', 'dst': '*'},
        ]
    }


    _packageNames= [ 'com.android.contacts', 'com.android.incallui']

    _mapping= {

        'Press Dial Button': 'call',
        'Press Hangup Button': 'hangup',
        'Call Ended': 'call_ended',
        'Incoming Call': 'incoming_call',
        'Event call in progress or accept call': 'call_in_progress',
        '*': 'wild'
     }

    def trigger(self, event, *args, **kwargs):
        '''
            Triggers the given event.
            The event can be triggered by calling the event handler directly, for ex: fsm.eat()
            but this method will come in handy if the event is determined dynamically and you have
            the event name to trigger as a string.
        '''




        if not hasattr(self, event):
            raise FysomError(
                "There isn't any event registered as %s" % event)
        return getattr(self, event)(*args, **kwargs)


    def out(self,msg):
        """

        :param msg:
        :return:
        """
        print msg

    def onchangestate(self,e):
        """

        :return:
        """
        #print e


    def onCALLING(self,e):
        """

        :return:
        """
        print "enter calling"
        print ">> click   ${userA}    resourceId=com.android.contacts:id/dialButton"


    def onINCOMING(self,e):
        """

        :return:
        """
        print "enter incoming"


    def onbeforecall_in_progress(self,e):
        """
            receive event TYPE_NOTIFICATION_STATE_CHANGED ( contentView=com.android.incallui/0x7f040065)

            if current state is  "INCOMING"
                > generate the answer call OK
        :param e:
        :return:
        """
        if self.current=="INCOMING":
            print ">> Macro Phone Answer Call     ${userB}"

    def onbeforehangup(self,e):
        """

        :param e:
        :return:
        """
        print ">> Macro Phone hangup call     ${userA}"


    def onCONFIRMED(self,e):
        """

        :return:
        """
        print "call confirmed"



# fsm = CallFsm({ 'initial': 'null',
#               'events': [
#                   {'name': 'call', 'src': '*', 'dst': 'CALLING'},
#                   {'name': 'hangup', 'src': 'CONFIRMED', 'dst': 'DISCONNECTED'},
#                   {'name': 'incoming', 'src': '*', 'dst': 'INCOMING'},
#                   {'name': 'call_in_progress', 'src': ['CALLING','INCOMING'], 'dst': 'CONFIRMED'} ,
#                   {'name': 'reset', 'src': '*', 'dst': 'NULL'},
#                   {'name': 'call_ended', 'src': 'CONFIRMED', 'dst': 'NULL'},
#                   {'name': 'wild', 'src': '*', 'dst': '*'},
#
#                 ] })


#Macro Phone Answer Call     ${userB}


fsm = CallFsm()



fsm.call(arguments=dict(name="press dial buttton",description="dial call"))

print fsm.current


fsm.call_in_progress()
print fsm.current

fsm.call_in_progress()
print fsm.current

fsm.wild()



fsm.call_ended()

print fsm.current


fsm.reset()
print fsm.current


fsm.incoming()

print fsm.current

fsm.call_in_progress()
print fsm.current

fsm.call_in_progress()
print fsm.current



fsm.hangup()
print fsm.current


fsm.reset()

try:
    fsm.call_in_progress()
except:
    pass
print fsm.current


print