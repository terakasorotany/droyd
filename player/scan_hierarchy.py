__author__ = 'cocoon'





import re
import json
from xml.etree import ElementTree



class RilServiceMod(object):
    """

        extract a dictionary of ril service data from a xml dump of the page

        list of properties
        option button = content-desc='More options'


    """
    # CA-DISABLED 'Unknown'
    keys=('LTE RRC','BAND','BW','MCC-MNC','TAC','Earfcn','PCI','RSRP','RSRQ','SNR','PA STATE','HDET','STATE','SUB','SERVICE',
           'REJECT','LU_ATT#','TMSI','NETWORK','IMEI Certi')

    arg_pattern=re.compile( r'(.*?)\:\s*([^, ]+)')

    line_resource_id =  "com.sec.android.RilServiceModeApp:id/testModeList"
    line_class= "android.widget.TextView"

    option_button= { 'content-desc':'More options','class': 'android.widget.ImageButton'}

    def __init__(self,root):
        """


        :param root: instance of element
        :return:
        """
        self.root=root


    @classmethod
    def from_xml_file(cls,filename):
        """

        :param filename: str , name of a xml file representing Ril service mode dump
        :return:
        """
        root = ElementTree.parse(filename).getroot()
        return cls(root)

    @classmethod
    def from_xml_string(cls,xml_string):
        """

        :param filename: str , name of a xml file representing Ril service mode dump
        :return:
        """
        root = ElementTree.fromstring(xml_string)
        return cls(root)

    @classmethod
    def from_json(cls,json_xml):
        """

        :param filename: str , name of a xml file representing Ril service mode dump
        :return:
        """
        xml_str= json.loads(json_xml)
        return cls.from_xml_string(xml_str)


    @property
    def data(self):
        """
            return data dict

        :return:
        """
        lines= self.find_text()
        data= self.extract_data(lines)
        return data


    # low level function

    def find_text(self,root=None):
        """

            yield  ril text lines in hierarchy

            class: android.widget.TextView
            resource-id: com.sec.android.RilServiceModeApp:id/testModeList


        :param root: instance of Element
        :return: yield line element
        """
        if not root:
            root=self.root
        for e in root.iter():
            if e.tag =="node":
                if e.attrib['class'] != self.line_class:  #"android.widget.TextView"
                    # not a text view
                    continue
                if e.attrib['resource-id'] != self.line_resource_id:  #'com.sec.android.RilServiceModeApp:id/testModeList'
                    # not the good kind
                    continue
                # we found a corresponding element yield the 'text' attribute
                yield e.attrib['text']

    @classmethod
    def extract_data(cls,lines):
        """

            extract keys from ril lines

        :param lines:
        :return:
        """
        keys={
            '_others':[]
        }

        for line in lines:
            #print line
            if "CA-DISABLED" in line:
                # handle 'CA-DISABLED'
                keys['CA-DISABLED'] = True
            elif "IMEI Certi" in line:
                # handle 'IMEI Certi: PASS, 1'
                key,value=line.split(':')
                keys[key.strip()]= value.strip()
            else:
                # general cases  key: value
                matches= cls.arg_pattern.findall(line)
                if matches:
                    for m in matches:
                        keys[m[0].strip(", ")]= m[1].strip()
                else:
                    # unkown
                    if line.strip():
                        keys['_others'].append(line.strip())
        return keys





if __name__=="__main__":


    dump_sample= "./data/dump_1072803595325867781.uix"

    # ril = RilServiceMod.from_xml_file(dump_sample)
    #
    # lines = ril.find_text()
    # #print list(lines)
    #
    # data = ril.extract_data(lines)
    # print data


    data= RilServiceMod.from_xml_file(dump_sample).data
    print data

    with open(dump_sample,'r') as fh:

        xml_str= fh.read()

        data= RilServiceMod.from_xml_string(xml_str).data

        print data


