__author__ = 'cocoon'

from droydhub.interface import DroydHub






Alice =   "e7f54be6"
Bob   = 	"388897e5"

url = "http://127.0.0.1:5000"



quick_launch_phone = dict( className = "android.widget.TextView" , packageName = "com.sec.android.app.launcher" , text="Phone")



# quick settings
air_plane_mode = dict(  packageName="com.android.systemui", className="android.widget.LinearLayout" , index="15")

quick_settings_OK = dict( packageName="com.android.systemui", className="android.widget.Button" , text= "OK")
quick_settings_CANCEL = dict( packageName="com.android.systemui", className="android.widget.Button" , text= "Cancel")

quick_settings_Title = dict( packageName="com.android.systemui", className="android.widget.TextView" , resourceId="android:id/alertTitle")
air_plane_text_on = "Turn on Airplane mode"
air_plane_text_off = "Airplane mode"




def turn_on_airplane(hub , user):
    """
    :param hub:
    :param user:
    :return:
    """
    hub.press_home(user)

    hub.open_quick_settings(user )
    # click on airplane logo
    hub.click(user, **air_plane_mode)
    # check presence of airplane on popup
    presence = hub.wait_for_exists( user ,timeout= 1000, text = air_plane_text_on , ** quick_settings_Title)
    if presence:
        # click OK
        hub.click( user , ** quick_settings_OK )
    else:
        # click cancel : already in airplane mode
        hub.click( user , ** quick_settings_CANCEL )


def turn_off_airplane(hub , user):
    """
    :param hub:
    :param user:
    :return:
    """
    hub.press_home(user)

    hub.open_quick_settings(user )
    # click on airplane logo
    hub.click(user, **air_plane_mode)
    # check presence of airplane on popup
    presence = hub.wait_for_exists( user ,timeout= 1000, text = air_plane_text_on , ** quick_settings_Title)
    if presence:
        # click CANCEL
        hub.click( user , ** quick_settings_CANCEL )
    else:
        # click cancel :
        hub.click( user , ** quick_settings_OK )




def hub_test(url):


    h = DroydHub(url)


    h.open_session(Alice,Bob,)
    info = h.get_device_info(Alice)


    h.press_home(Alice)

    presence = h.wait_for_exists(Alice,timeout=1000,**quick_launch_phone)
    if presence:
        h.click(Alice , **quick_launch_phone)
    else:
        print "failed to find quick_launch_phone selector"


    #turn_on_airplane(h,Bob)

    turn_off_airplane(h,Bob)



    try:
        h.bad_function(Alice, "other" )
    except AttributeError:
        pass

    print


def native_test():


    return hub_test(url=None)


def http_test():

    return hub_test(url = url)




## begins here
#mock_test()
#native_test()

http_test()

print
