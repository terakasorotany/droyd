from xml.etree import ElementTree

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i



if __name__=="__main__":

    # hierarchy= "../doc/dump_hierarchy_sample.xml"
    hierarchy= "./data/dump_1072803595325867781.uix"
    root = ElementTree.parse(hierarchy).getroot()
    indent(root)
    ElementTree.dump(root)


