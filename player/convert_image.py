__author__ = 'cocoon'


import base64



png_file="./data/dump_1072803595325867781.png"


# create base 64 image
with open(png_file, "rb") as f:
    data = f.read()
    encoded= data.encode("base64")

    with open("./data/image.base64","wb") as fh:
        fh.write(encoded)



with open("./data/image.base64", "rb") as fh:

    data = fh.read()
    hex= data.decode("base64")

    with open("./data/recreated.png","wb") as fw:
        fw.write(hex)


# string = base64.b64encode(data)
# convert = base64.b64decode(string)
#
# t = open("example.png", "w+")
# t.write(convert)
# t.close()